<?xml version="1.0"?>
<api version="1.0">
	<namespace name="GUPnP">
		<object name="GUPnPDLNADiscoverer" parent="GstDiscoverer" type-name="GUPnPDLNADiscoverer" get-type="gupnp_dlna_discoverer_get_type">
			<method name="discover_uri" symbol="gupnp_dlna_discoverer_discover_uri">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="discoverer" type="GUPnPDLNADiscoverer*"/>
					<parameter name="uri" type="gchar*"/>
				</parameters>
			</method>
			<method name="discover_uri_sync" symbol="gupnp_dlna_discoverer_discover_uri_sync">
				<return-type type="GUPnPDLNAInformation*"/>
				<parameters>
					<parameter name="discoverer" type="GUPnPDLNADiscoverer*"/>
					<parameter name="uri" type="gchar*"/>
					<parameter name="err" type="GError**"/>
				</parameters>
			</method>
			<method name="get_extended_mode" symbol="gupnp_dlna_discoverer_get_extended_mode">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="self" type="GUPnPDLNADiscoverer*"/>
				</parameters>
			</method>
			<method name="get_profile" symbol="gupnp_dlna_discoverer_get_profile">
				<return-type type="GUPnPDLNAProfile*"/>
				<parameters>
					<parameter name="self" type="GUPnPDLNADiscoverer*"/>
					<parameter name="name" type="gchar*"/>
				</parameters>
			</method>
			<method name="get_relaxed_mode" symbol="gupnp_dlna_discoverer_get_relaxed_mode">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="self" type="GUPnPDLNADiscoverer*"/>
				</parameters>
			</method>
			<method name="list_profiles" symbol="gupnp_dlna_discoverer_list_profiles">
				<return-type type="GList*"/>
				<parameters>
					<parameter name="self" type="GUPnPDLNADiscoverer*"/>
				</parameters>
			</method>
			<constructor name="new" symbol="gupnp_dlna_discoverer_new">
				<return-type type="GUPnPDLNADiscoverer*"/>
				<parameters>
					<parameter name="timeout" type="GstClockTime"/>
					<parameter name="relaxed_mode" type="gboolean"/>
					<parameter name="extended_mode" type="gboolean"/>
				</parameters>
			</constructor>
			<property name="extended-mode" type="gboolean" readable="1" writable="1" construct="0" construct-only="1"/>
			<property name="relaxed-mode" type="gboolean" readable="1" writable="1" construct="0" construct-only="1"/>
			<signal name="done" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="discoverer" type="GUPnPDLNADiscoverer*"/>
					<parameter name="dlna" type="GUPnPDLNAInformation*"/>
					<parameter name="err" type="GError*"/>
				</parameters>
			</signal>
		</object>
		<object name="GUPnPDLNAInformation" parent="GObject" type-name="GUPnPDLNAInformation" get-type="gupnp_dlna_information_get_type">
			<method name="get_info" symbol="gupnp_dlna_information_get_info">
				<return-type type="GstDiscovererInfo*"/>
				<parameters>
					<parameter name="self" type="GUPnPDLNAInformation*"/>
				</parameters>
			</method>
			<method name="get_mime" symbol="gupnp_dlna_information_get_mime">
				<return-type type="gchar*"/>
				<parameters>
					<parameter name="self" type="GUPnPDLNAInformation*"/>
				</parameters>
			</method>
			<method name="get_name" symbol="gupnp_dlna_information_get_name">
				<return-type type="gchar*"/>
				<parameters>
					<parameter name="self" type="GUPnPDLNAInformation*"/>
				</parameters>
			</method>
			<constructor name="new" symbol="gupnp_dlna_information_new">
				<return-type type="GUPnPDLNAInformation*"/>
				<parameters>
					<parameter name="name" type="gchar*"/>
					<parameter name="mime" type="gchar*"/>
					<parameter name="info" type="GstDiscovererInfo*"/>
				</parameters>
			</constructor>
			<constructor name="new_from_discoverer_info" symbol="gupnp_dlna_information_new_from_discoverer_info">
				<return-type type="GUPnPDLNAInformation*"/>
				<parameters>
					<parameter name="info" type="GstDiscovererInfo*"/>
					<parameter name="profiles" type="GList*"/>
				</parameters>
			</constructor>
			<property name="info" type="GstDiscovererInfo" readable="1" writable="1" construct="0" construct-only="1"/>
			<property name="mime" type="char*" readable="1" writable="1" construct="0" construct-only="1"/>
			<property name="name" type="char*" readable="1" writable="1" construct="0" construct-only="1"/>
		</object>
		<object name="GUPnPDLNAProfile" parent="GObject" type-name="GUPnPDLNAProfile" get-type="gupnp_dlna_profile_get_type">
			<method name="get_encoding_profile" symbol="gupnp_dlna_profile_get_encoding_profile">
				<return-type type="GstEncodingProfile*"/>
				<parameters>
					<parameter name="self" type="GUPnPDLNAProfile*"/>
				</parameters>
			</method>
			<method name="get_extended" symbol="gupnp_dlna_profile_get_extended">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="self" type="GUPnPDLNAProfile*"/>
				</parameters>
			</method>
			<method name="get_mime" symbol="gupnp_dlna_profile_get_mime">
				<return-type type="gchar*"/>
				<parameters>
					<parameter name="self" type="GUPnPDLNAProfile*"/>
				</parameters>
			</method>
			<method name="get_name" symbol="gupnp_dlna_profile_get_name">
				<return-type type="gchar*"/>
				<parameters>
					<parameter name="self" type="GUPnPDLNAProfile*"/>
				</parameters>
			</method>
			<property name="encoding-profile" type="GstEncodingProfile" readable="1" writable="0" construct="0" construct-only="0"/>
			<property name="extended" type="gboolean" readable="1" writable="1" construct="0" construct-only="1"/>
			<property name="mime" type="char*" readable="1" writable="1" construct="0" construct-only="1"/>
			<property name="name" type="char*" readable="1" writable="1" construct="0" construct-only="1"/>
		</object>
	</namespace>
</api>
