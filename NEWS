0.10.5
======

Changes since 0.10.4:

- Disable generation of vapi for all projects that can do it on their own.

All contributors:

Jens Georg <mail@jensge.org>

0.10.4
======

Changes since 0.10.3:

- Only generate VAPI if GSSDP version is < 0.12.2.
- Remove --thread argument from examples.

All Contributors:

Jens Georg <mail@jensge.org>

0.10.3
======

Changes since 0.10.2:

- Fix ownership of Discoverer.get_profile.
- Update autotools.
- Update autogen.sh.
- Fix bindings for send_action_{list,hash} and end_action_{list,hash}.
- (Hopefully) fix rebuild issues.

0.10.2
======

Changes since 0.10.1:

- Fix ownership of GUPnP.ContextManager.create.
- Update autotools scripts.

All contributors:

Jens Georg <mail@jensge.org>
Javier Jardón <jjardon@gnome.org>

0.10.1
======

Changes since 0.10.0:

- Remove gupnp-ui support.

All contributors:

Zeeshan Ali (Khattak) <zeeshanak@gnome.org>


0.10.0
======

This release brings 0.9.x into a stable release cycle.

- Require and adapt to:
  gupnp >= 0.18.0.

All contributors:

Jens Georg <mail@jensge.org>

0.9.0
=====

- Require and adapt to:
  gssdp >= 0.11.0.
  gupnp >= 0.17.0.
  gupnp-av >= 0.9.0.

All contributors:

Zeeshan Ali (Khattak) <zeeshanak@gnome.org>
Jens Georg <mail@jensge.org>
Topi Santakivi <topi.santakivi@digia.com>

0.8.0
=====

This release brings 0.7.x into a stable release cycle.

All contributors:

Zeeshan Ali (Khattak) <zeeshanak@gnome.org>
Luis de Bethencourt <luis@debethencourt.com>

0.7.5
=====

- Require and adapt to gssdp >= 0.9.2.

All contributors:

Zeeshan Ali (Khattak) <zeeshanak@gnome.org>

0.7.4
=====

Another micro release that fixes a regression in 0.7.3.

All contributors:

Zeeshan Ali (Khattak) <zeeshanak@gnome.org>

0.7.3
=====

- Require and adapt to gupnp-dlna >= 0.5.1.

All contributors:

Zeeshan Ali (Khattak) <zeeshanak@gnome.org>

0.7.2
=====

- Require and adapt to gupnp-dlna >= 0.5.
- Require vala >= 0.11.3.

All contributors:

Zeeshan Ali (Khattak) <zeeshanak@gnome.org>
Arun Raghavan <arun.raghavan@collabora.co.uk>

0.7.1
=====

Another micro release to fix the broken checks for Vala in the last release.

All contributors:

Zeeshan Ali (Khattak) <zeeshanak@gnome.org>

0.7.0
====

- Require and adapt to gupnp-av 0.7.0.
- Add/use m4 magic for Vala checks.

All contributors:

Zeeshan Ali (Khattak) <zeeshanak@gnome.org>
Marc-Andre Lureau <marcandre.lureau@gmail.com>

0.6.12
======

- Install VAPIs in standard VAPI directory.

Zeeshan Ali (Khattak) <zeeshanak@gnome.org>

0.6.11
======

- Require and adapt to gupnp-dlna 0.3.0.
- Hide internal gupnp_dlna_information_new_from_discoverer_info().

All contributors:

Zeeshan Ali (Khattak) <zeeshanak@gnome.org>
Parthasarathi Susarla <partha.susarla@collabora.co.uk>
Arun Raghavan <arun.raghavan@collabora.co.uk>

0.6.10
======

- Require and adapt to Vala 0.9.5.
- Require & adapt to gupnp-av 0.5.9.

All contributors:

Zeeshan Ali (Khattak) <zeeshanak@gnome.org>

0.6.9
=====

- Update test apps according to latest vala (0.9.3)
- Add bindings for gupnp-dlna.

All contributors:

Arun Raghavan <arun.raghavan@collabora.co.uk>
Zeeshan Ali (Khattak) <zeeshanak@gnome.org>

0.6.7
=====

- Require & adapt to gupnp-av 0.5.7.

All contributors:

Zeeshan Ali (Khattak) <zeeshanak@gnome.org>

0.6.6
=====

Mirco release to fix the broken tarball for last release.

All contributors:

Zeeshan Ali (Khattak) <zeeshanak@gnome.org>

0.6.5
=====

Changes since 0.6.4:

- Require & adapt to:
  - gssdp 0.7.2
  - gupnp 0.13.3
  - gupnp-av 0.5.5
  - vala 0.8.0
- Don't hide gupnp_root_device_get_relative_location

All contributors:

Zeeshan Ali (Khattak) <zeeshanak@gnome.org>

0.6.4
=====

Changes since 0.6.2:

- Require and adapt to gupnp-av 0.5.3.

All contributors:

Zeeshan Ali (Khattak) <zeeshanak@gnome.org>

0.6.2
=====

Changes since 0.6:

- Remove bogus type_argument from gupnp metadata.
- No need for custom bindings for SearchCriteriaParser.

Dependency-related changes:

- Require vala >= 0.7.8.
- Require and adapt to gupnp-av >= 0.5.2.

Bugs fixed:

1850 - Vapi for ServiceAction.get_message is wrong

All contributors:

Zeeshan Ali (Khattak) <zeeshanak@gnome.org>
Jens Georg <mail@jensge.org>

0.6
===

Changes since 0.5.4:

- Adapt to new gssdp, gupnp and gupnp-av APIs.
- Fix pkg-config paths.

Bugs fixed:

1570 - gupnp doesn't set the pkgconfig lib dir correctly in 64 bit env

Dependencies changed:

- gssdp >= 0.7
- gupnp >= 0.13
- gupnp-av >= 0.5

All contributors:

Zeeshan Ali (Khattak) <zeeshanak@gnome.org>
Ross Burton <ross@linux.intel.com>

WARNING: This release is API and ABI incompatible with previous releases.

0.5.4
=====

A new minor release mainly to update the gupnp-av bindings.

0.5.3
=====

- Service::action_invoked.action is now 'owned' by the handler.
- Remove all generated files on `make clean`.
- Tell Vala that ServiceAction doesn't have ref/unref functions.
- Add a pkg-config file for gupnp-vala.
- Require latest vala, gssdp and gupnp.

0.5.2
=====

- Require latest (0.5.6) vala to get compatible vapi files.
- Require latest of all GUPnP libraries.

0.5.1
=====

A minor release to put newer vapi files in the distribution tarball.

0.5
===

DIDLLiteResource API updates: No need to keep weak strings in the struct now
that _copy() and _destroy() functions are provided for this struct.

0.4
===

- update to GSSDP 0.6.3
- modify bindings to be compatible with Vala 0.5.3
- Updates to test apps

0.3
===

- modify bindings to be compatible with Vala 0.4 & 0.5
- Make GUPnPDIDLLiteResource fields weak
- Update bindings to GUPnP 0.12.3

0.2
===

- custom types available 
- Treat Service::query_variable.value as 'ref'
- requires Vala 0.3.4

0.1
===

Initial release of gupnp-vala. Includes Vala bindings for the whole gupnp stack:
 * gssdp-1.0 0.6.1
 * gupnp-1.0 0.12
 * gupnp-ui-1.0 0.1
 * gupnp-av-1.0 0.2.1

- Requires Vala 0.3.3
- custom GUPnP types (like GUPnPURI) are not supported yet, use plain strings
