<?xml version="1.0"?>
<api version="1.0">
	<namespace name="GSSDP">
		<function name="error_quark" symbol="gssdp_error_quark">
			<return-type type="GQuark"/>
		</function>
		<enum name="GSSDPError">
			<member name="GSSDP_ERROR_NO_IP_ADDRESS" value="0"/>
			<member name="GSSDP_ERROR_FAILED" value="1"/>
		</enum>
		<object name="GSSDPClient" parent="GObject" type-name="GSSDPClient" get-type="gssdp_client_get_type">
			<implements>
				<interface name="GInitable"/>
			</implements>
			<method name="get_active" symbol="gssdp_client_get_active">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="client" type="GSSDPClient*"/>
				</parameters>
			</method>
			<method name="get_host_ip" symbol="gssdp_client_get_host_ip">
				<return-type type="char*"/>
				<parameters>
					<parameter name="client" type="GSSDPClient*"/>
				</parameters>
			</method>
			<method name="get_interface" symbol="gssdp_client_get_interface">
				<return-type type="char*"/>
				<parameters>
					<parameter name="client" type="GSSDPClient*"/>
				</parameters>
			</method>
			<method name="get_main_context" symbol="gssdp_client_get_main_context">
				<return-type type="GMainContext*"/>
				<parameters>
					<parameter name="client" type="GSSDPClient*"/>
				</parameters>
			</method>
			<method name="get_network" symbol="gssdp_client_get_network">
				<return-type type="char*"/>
				<parameters>
					<parameter name="client" type="GSSDPClient*"/>
				</parameters>
			</method>
			<method name="get_server_id" symbol="gssdp_client_get_server_id">
				<return-type type="char*"/>
				<parameters>
					<parameter name="client" type="GSSDPClient*"/>
				</parameters>
			</method>
			<constructor name="new" symbol="gssdp_client_new">
				<return-type type="GSSDPClient*"/>
				<parameters>
					<parameter name="main_context" type="GMainContext*"/>
					<parameter name="iface" type="char*"/>
					<parameter name="error" type="GError**"/>
				</parameters>
			</constructor>
			<method name="set_network" symbol="gssdp_client_set_network">
				<return-type type="void"/>
				<parameters>
					<parameter name="client" type="GSSDPClient*"/>
					<parameter name="network" type="char*"/>
				</parameters>
			</method>
			<method name="set_server_id" symbol="gssdp_client_set_server_id">
				<return-type type="void"/>
				<parameters>
					<parameter name="client" type="GSSDPClient*"/>
					<parameter name="server_id" type="char*"/>
				</parameters>
			</method>
			<property name="active" type="gboolean" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="host-ip" type="char*" readable="1" writable="0" construct="0" construct-only="0"/>
			<property name="interface" type="char*" readable="1" writable="1" construct="0" construct-only="1"/>
			<property name="main-context" type="gpointer" readable="1" writable="1" construct="0" construct-only="1"/>
			<property name="network" type="char*" readable="1" writable="1" construct="1" construct-only="0"/>
			<property name="server-id" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
			<signal name="message-received" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="object" type="GSSDPClient*"/>
					<parameter name="p0" type="char*"/>
					<parameter name="p1" type="guint"/>
					<parameter name="p2" type="gint"/>
					<parameter name="p3" type="gpointer"/>
				</parameters>
			</signal>
		</object>
		<object name="GSSDPResourceBrowser" parent="GObject" type-name="GSSDPResourceBrowser" get-type="gssdp_resource_browser_get_type">
			<method name="get_active" symbol="gssdp_resource_browser_get_active">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="resource_browser" type="GSSDPResourceBrowser*"/>
				</parameters>
			</method>
			<method name="get_client" symbol="gssdp_resource_browser_get_client">
				<return-type type="GSSDPClient*"/>
				<parameters>
					<parameter name="resource_browser" type="GSSDPResourceBrowser*"/>
				</parameters>
			</method>
			<method name="get_mx" symbol="gssdp_resource_browser_get_mx">
				<return-type type="gushort"/>
				<parameters>
					<parameter name="resource_browser" type="GSSDPResourceBrowser*"/>
				</parameters>
			</method>
			<method name="get_target" symbol="gssdp_resource_browser_get_target">
				<return-type type="char*"/>
				<parameters>
					<parameter name="resource_browser" type="GSSDPResourceBrowser*"/>
				</parameters>
			</method>
			<constructor name="new" symbol="gssdp_resource_browser_new">
				<return-type type="GSSDPResourceBrowser*"/>
				<parameters>
					<parameter name="client" type="GSSDPClient*"/>
					<parameter name="target" type="char*"/>
				</parameters>
			</constructor>
			<method name="set_active" symbol="gssdp_resource_browser_set_active">
				<return-type type="void"/>
				<parameters>
					<parameter name="resource_browser" type="GSSDPResourceBrowser*"/>
					<parameter name="active" type="gboolean"/>
				</parameters>
			</method>
			<method name="set_mx" symbol="gssdp_resource_browser_set_mx">
				<return-type type="void"/>
				<parameters>
					<parameter name="resource_browser" type="GSSDPResourceBrowser*"/>
					<parameter name="mx" type="gushort"/>
				</parameters>
			</method>
			<method name="set_target" symbol="gssdp_resource_browser_set_target">
				<return-type type="void"/>
				<parameters>
					<parameter name="resource_browser" type="GSSDPResourceBrowser*"/>
					<parameter name="target" type="char*"/>
				</parameters>
			</method>
			<property name="active" type="gboolean" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="client" type="GSSDPClient*" readable="1" writable="1" construct="0" construct-only="1"/>
			<property name="mx" type="guint" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="target" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
			<signal name="resource-available" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="resource_browser" type="GSSDPResourceBrowser*"/>
					<parameter name="usn" type="char*"/>
					<parameter name="locations" type="gpointer"/>
				</parameters>
			</signal>
			<signal name="resource-unavailable" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="resource_browser" type="GSSDPResourceBrowser*"/>
					<parameter name="usn" type="char*"/>
				</parameters>
			</signal>
		</object>
		<object name="GSSDPResourceGroup" parent="GObject" type-name="GSSDPResourceGroup" get-type="gssdp_resource_group_get_type">
			<method name="add_resource" symbol="gssdp_resource_group_add_resource">
				<return-type type="guint"/>
				<parameters>
					<parameter name="resource_group" type="GSSDPResourceGroup*"/>
					<parameter name="target" type="char*"/>
					<parameter name="usn" type="char*"/>
					<parameter name="locations" type="GList*"/>
				</parameters>
			</method>
			<method name="add_resource_simple" symbol="gssdp_resource_group_add_resource_simple">
				<return-type type="guint"/>
				<parameters>
					<parameter name="resource_group" type="GSSDPResourceGroup*"/>
					<parameter name="target" type="char*"/>
					<parameter name="usn" type="char*"/>
					<parameter name="location" type="char*"/>
				</parameters>
			</method>
			<method name="get_available" symbol="gssdp_resource_group_get_available">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="resource_group" type="GSSDPResourceGroup*"/>
				</parameters>
			</method>
			<method name="get_client" symbol="gssdp_resource_group_get_client">
				<return-type type="GSSDPClient*"/>
				<parameters>
					<parameter name="resource_group" type="GSSDPResourceGroup*"/>
				</parameters>
			</method>
			<method name="get_max_age" symbol="gssdp_resource_group_get_max_age">
				<return-type type="guint"/>
				<parameters>
					<parameter name="resource_group" type="GSSDPResourceGroup*"/>
				</parameters>
			</method>
			<method name="get_message_delay" symbol="gssdp_resource_group_get_message_delay">
				<return-type type="guint"/>
				<parameters>
					<parameter name="resource_group" type="GSSDPResourceGroup*"/>
				</parameters>
			</method>
			<constructor name="new" symbol="gssdp_resource_group_new">
				<return-type type="GSSDPResourceGroup*"/>
				<parameters>
					<parameter name="client" type="GSSDPClient*"/>
				</parameters>
			</constructor>
			<method name="remove_resource" symbol="gssdp_resource_group_remove_resource">
				<return-type type="void"/>
				<parameters>
					<parameter name="resource_group" type="GSSDPResourceGroup*"/>
					<parameter name="resource_id" type="guint"/>
				</parameters>
			</method>
			<method name="set_available" symbol="gssdp_resource_group_set_available">
				<return-type type="void"/>
				<parameters>
					<parameter name="resource_group" type="GSSDPResourceGroup*"/>
					<parameter name="available" type="gboolean"/>
				</parameters>
			</method>
			<method name="set_max_age" symbol="gssdp_resource_group_set_max_age">
				<return-type type="void"/>
				<parameters>
					<parameter name="resource_group" type="GSSDPResourceGroup*"/>
					<parameter name="max_age" type="guint"/>
				</parameters>
			</method>
			<method name="set_message_delay" symbol="gssdp_resource_group_set_message_delay">
				<return-type type="void"/>
				<parameters>
					<parameter name="resource_group" type="GSSDPResourceGroup*"/>
					<parameter name="message_delay" type="guint"/>
				</parameters>
			</method>
			<property name="available" type="gboolean" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="client" type="GSSDPClient*" readable="1" writable="1" construct="0" construct-only="1"/>
			<property name="max-age" type="guint" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="message-delay" type="guint" readable="1" writable="1" construct="0" construct-only="0"/>
		</object>
		<constant name="GSSDP_ALL_RESOURCES" type="char*" value="ssdp:all"/>
	</namespace>
</api>
