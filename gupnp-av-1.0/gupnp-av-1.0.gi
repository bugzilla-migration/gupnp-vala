<?xml version="1.0"?>
<api version="1.0">
	<namespace name="GUPnP">
		<function name="protocol_error_quark" symbol="gupnp_protocol_error_quark">
			<return-type type="GQuark"/>
		</function>
		<enum name="GUPnPProtocolError">
			<member name="GUPNP_PROTOCOL_ERROR_INVALID_SYNTAX" value="0"/>
			<member name="GUPNP_PROTOCOL_ERROR_OTHER" value="1"/>
		</enum>
		<enum name="GUPnPSearchCriteriaOp" type-name="GUPnPSearchCriteriaOp" get-type="gupnp_search_criteria_op_get_type">
			<member name="GUPNP_SEARCH_CRITERIA_OP_EQ" value="271"/>
			<member name="GUPNP_SEARCH_CRITERIA_OP_NEQ" value="272"/>
			<member name="GUPNP_SEARCH_CRITERIA_OP_LESS" value="273"/>
			<member name="GUPNP_SEARCH_CRITERIA_OP_LEQ" value="274"/>
			<member name="GUPNP_SEARCH_CRITERIA_OP_GREATER" value="275"/>
			<member name="GUPNP_SEARCH_CRITERIA_OP_GEQ" value="276"/>
			<member name="GUPNP_SEARCH_CRITERIA_OP_CONTAINS" value="277"/>
			<member name="GUPNP_SEARCH_CRITERIA_OP_DOES_NOT_CONTAIN" value="278"/>
			<member name="GUPNP_SEARCH_CRITERIA_OP_DERIVED_FROM" value="279"/>
			<member name="GUPNP_SEARCH_CRITERIA_OP_EXISTS" value="280"/>
		</enum>
		<enum name="GUPnPSearchCriteriaParserError">
			<member name="GUPNP_SEARCH_CRITERIA_PARSER_ERROR_FAILED" value="0"/>
		</enum>
		<flags name="GUPnPDLNAConversion" type-name="GUPnPDLNAConversion" get-type="gupnp_dlna_conversion_get_type">
			<member name="GUPNP_DLNA_CONVERSION_NONE" value="0"/>
			<member name="GUPNP_DLNA_CONVERSION_TRANSCODED" value="1"/>
		</flags>
		<flags name="GUPnPDLNAFlags" type-name="GUPnPDLNAFlags" get-type="gupnp_dlna_flags_get_type">
			<member name="GUPNP_DLNA_FLAGS_NONE" value="0"/>
			<member name="GUPNP_DLNA_FLAGS_SENDER_PACED" value="-2147483648"/>
			<member name="GUPNP_DLNA_FLAGS_TIME_BASED_SEEK" value="1073741824"/>
			<member name="GUPNP_DLNA_FLAGS_BYTE_BASED_SEEK" value="536870912"/>
			<member name="GUPNP_DLNA_FLAGS_PLAY_CONTAINER" value="268435456"/>
			<member name="GUPNP_DLNA_FLAGS_S0_INCREASE" value="134217728"/>
			<member name="GUPNP_DLNA_FLAGS_SN_INCREASE" value="67108864"/>
			<member name="GUPNP_DLNA_FLAGS_RTSP_PAUSE" value="33554432"/>
			<member name="GUPNP_DLNA_FLAGS_STREAMING_TRANSFER_MODE" value="16777216"/>
			<member name="GUPNP_DLNA_FLAGS_INTERACTIVE_TRANSFER_MODE" value="8388608"/>
			<member name="GUPNP_DLNA_FLAGS_BACKGROUND_TRANSFER_MODE" value="4194304"/>
			<member name="GUPNP_DLNA_FLAGS_CONNECTION_STALL" value="2097152"/>
			<member name="GUPNP_DLNA_FLAGS_DLNA_V15" value="1048576"/>
		</flags>
		<flags name="GUPnPDLNAOperation" type-name="GUPnPDLNAOperation" get-type="gupnp_dlna_operation_get_type">
			<member name="GUPNP_DLNA_OPERATION_NONE" value="0"/>
			<member name="GUPNP_DLNA_OPERATION_RANGE" value="1"/>
			<member name="GUPNP_DLNA_OPERATION_TIMESEEK" value="16"/>
		</flags>
		<flags name="GUPnPOCMFlags" type-name="GUPnPOCMFlags" get-type="gupnp_ocm_flags_get_type">
			<member name="GUPNP_OCM_FLAGS_NONE" value="0"/>
			<member name="GUPNP_OCM_FLAGS_UPLOAD" value="1"/>
			<member name="GUPNP_OCM_FLAGS_CREATE_CONTAINER" value="2"/>
			<member name="GUPNP_OCM_FLAGS_DESTROYABLE" value="4"/>
			<member name="GUPNP_OCM_FLAGS_UPLOAD_DESTROYABLE" value="8"/>
			<member name="GUPNP_OCM_FLAGS_CHANGE_METADATA" value="16"/>
		</flags>
		<object name="GUPnPDIDLLiteContainer" parent="GUPnPDIDLLiteObject" type-name="GUPnPDIDLLiteContainer" get-type="gupnp_didl_lite_container_get_type">
			<method name="add_create_class" symbol="gupnp_didl_lite_container_add_create_class">
				<return-type type="void"/>
				<parameters>
					<parameter name="container" type="GUPnPDIDLLiteContainer*"/>
					<parameter name="create_class" type="char*"/>
				</parameters>
			</method>
			<method name="add_create_class_full" symbol="gupnp_didl_lite_container_add_create_class_full">
				<return-type type="void"/>
				<parameters>
					<parameter name="container" type="GUPnPDIDLLiteContainer*"/>
					<parameter name="create_class" type="char*"/>
					<parameter name="include_derived" type="gboolean"/>
				</parameters>
			</method>
			<method name="add_search_class" symbol="gupnp_didl_lite_container_add_search_class">
				<return-type type="void"/>
				<parameters>
					<parameter name="container" type="GUPnPDIDLLiteContainer*"/>
					<parameter name="search_class" type="char*"/>
				</parameters>
			</method>
			<method name="add_search_class_full" symbol="gupnp_didl_lite_container_add_search_class_full">
				<return-type type="void"/>
				<parameters>
					<parameter name="container" type="GUPnPDIDLLiteContainer*"/>
					<parameter name="search_class" type="char*"/>
					<parameter name="include_derived" type="gboolean"/>
				</parameters>
			</method>
			<method name="get_child_count" symbol="gupnp_didl_lite_container_get_child_count">
				<return-type type="gint"/>
				<parameters>
					<parameter name="container" type="GUPnPDIDLLiteContainer*"/>
				</parameters>
			</method>
			<method name="get_create_classes" symbol="gupnp_didl_lite_container_get_create_classes">
				<return-type type="GList*"/>
				<parameters>
					<parameter name="container" type="GUPnPDIDLLiteContainer*"/>
				</parameters>
			</method>
			<method name="get_search_classes" symbol="gupnp_didl_lite_container_get_search_classes">
				<return-type type="GList*"/>
				<parameters>
					<parameter name="container" type="GUPnPDIDLLiteContainer*"/>
				</parameters>
			</method>
			<method name="get_searchable" symbol="gupnp_didl_lite_container_get_searchable">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="container" type="GUPnPDIDLLiteContainer*"/>
				</parameters>
			</method>
			<method name="get_storage_used" symbol="gupnp_didl_lite_container_get_storage_used">
				<return-type type="gint64"/>
				<parameters>
					<parameter name="container" type="GUPnPDIDLLiteContainer*"/>
				</parameters>
			</method>
			<method name="set_child_count" symbol="gupnp_didl_lite_container_set_child_count">
				<return-type type="void"/>
				<parameters>
					<parameter name="container" type="GUPnPDIDLLiteContainer*"/>
					<parameter name="child_count" type="gint"/>
				</parameters>
			</method>
			<method name="set_searchable" symbol="gupnp_didl_lite_container_set_searchable">
				<return-type type="void"/>
				<parameters>
					<parameter name="container" type="GUPnPDIDLLiteContainer*"/>
					<parameter name="searchable" type="gboolean"/>
				</parameters>
			</method>
			<method name="set_storage_used" symbol="gupnp_didl_lite_container_set_storage_used">
				<return-type type="void"/>
				<parameters>
					<parameter name="container" type="GUPnPDIDLLiteContainer*"/>
					<parameter name="storage_used" type="gint64"/>
				</parameters>
			</method>
			<property name="child-count" type="gint" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="searchable" type="gboolean" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="storage-used" type="gint64" readable="1" writable="1" construct="0" construct-only="0"/>
		</object>
		<object name="GUPnPDIDLLiteContributor" parent="GObject" type-name="GUPnPDIDLLiteContributor" get-type="gupnp_didl_lite_contributor_get_type">
			<method name="get_name" symbol="gupnp_didl_lite_contributor_get_name">
				<return-type type="char*"/>
				<parameters>
					<parameter name="contributor" type="GUPnPDIDLLiteContributor*"/>
				</parameters>
			</method>
			<method name="get_role" symbol="gupnp_didl_lite_contributor_get_role">
				<return-type type="char*"/>
				<parameters>
					<parameter name="contributor" type="GUPnPDIDLLiteContributor*"/>
				</parameters>
			</method>
			<method name="get_xml_node" symbol="gupnp_didl_lite_contributor_get_xml_node">
				<return-type type="xmlNode*"/>
				<parameters>
					<parameter name="contributor" type="GUPnPDIDLLiteContributor*"/>
				</parameters>
			</method>
			<method name="set_name" symbol="gupnp_didl_lite_contributor_set_name">
				<return-type type="void"/>
				<parameters>
					<parameter name="contributor" type="GUPnPDIDLLiteContributor*"/>
					<parameter name="name" type="char*"/>
				</parameters>
			</method>
			<method name="set_role" symbol="gupnp_didl_lite_contributor_set_role">
				<return-type type="void"/>
				<parameters>
					<parameter name="contributor" type="GUPnPDIDLLiteContributor*"/>
					<parameter name="role" type="char*"/>
				</parameters>
			</method>
			<property name="name" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="role" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="xml-doc" type="GUPnPXMLDoc*" readable="0" writable="1" construct="0" construct-only="1"/>
			<property name="xml-node" type="gpointer" readable="1" writable="1" construct="0" construct-only="1"/>
		</object>
		<object name="GUPnPDIDLLiteDescriptor" parent="GObject" type-name="GUPnPDIDLLiteDescriptor" get-type="gupnp_didl_lite_descriptor_get_type">
			<method name="get_content" symbol="gupnp_didl_lite_descriptor_get_content">
				<return-type type="char*"/>
				<parameters>
					<parameter name="descriptor" type="GUPnPDIDLLiteDescriptor*"/>
				</parameters>
			</method>
			<method name="get_id" symbol="gupnp_didl_lite_descriptor_get_id">
				<return-type type="char*"/>
				<parameters>
					<parameter name="descriptor" type="GUPnPDIDLLiteDescriptor*"/>
				</parameters>
			</method>
			<method name="get_metadata_type" symbol="gupnp_didl_lite_descriptor_get_metadata_type">
				<return-type type="char*"/>
				<parameters>
					<parameter name="descriptor" type="GUPnPDIDLLiteDescriptor*"/>
				</parameters>
			</method>
			<method name="get_name_space" symbol="gupnp_didl_lite_descriptor_get_name_space">
				<return-type type="char*"/>
				<parameters>
					<parameter name="descriptor" type="GUPnPDIDLLiteDescriptor*"/>
				</parameters>
			</method>
			<method name="get_xml_node" symbol="gupnp_didl_lite_descriptor_get_xml_node">
				<return-type type="xmlNode*"/>
				<parameters>
					<parameter name="descriptor" type="GUPnPDIDLLiteDescriptor*"/>
				</parameters>
			</method>
			<method name="set_content" symbol="gupnp_didl_lite_descriptor_set_content">
				<return-type type="void"/>
				<parameters>
					<parameter name="descriptor" type="GUPnPDIDLLiteDescriptor*"/>
					<parameter name="content" type="char*"/>
				</parameters>
			</method>
			<method name="set_id" symbol="gupnp_didl_lite_descriptor_set_id">
				<return-type type="void"/>
				<parameters>
					<parameter name="descriptor" type="GUPnPDIDLLiteDescriptor*"/>
					<parameter name="id" type="char*"/>
				</parameters>
			</method>
			<method name="set_metadata_type" symbol="gupnp_didl_lite_descriptor_set_metadata_type">
				<return-type type="void"/>
				<parameters>
					<parameter name="descriptor" type="GUPnPDIDLLiteDescriptor*"/>
					<parameter name="type" type="char*"/>
				</parameters>
			</method>
			<method name="set_name_space" symbol="gupnp_didl_lite_descriptor_set_name_space">
				<return-type type="void"/>
				<parameters>
					<parameter name="descriptor" type="GUPnPDIDLLiteDescriptor*"/>
					<parameter name="name_space" type="char*"/>
				</parameters>
			</method>
			<property name="content" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="id" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="metadata-type" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="name-space" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="xml-doc" type="GUPnPXMLDoc*" readable="0" writable="1" construct="0" construct-only="1"/>
			<property name="xml-node" type="gpointer" readable="1" writable="1" construct="0" construct-only="1"/>
		</object>
		<object name="GUPnPDIDLLiteItem" parent="GUPnPDIDLLiteObject" type-name="GUPnPDIDLLiteItem" get-type="gupnp_didl_lite_item_get_type">
			<method name="get_ref_id" symbol="gupnp_didl_lite_item_get_ref_id">
				<return-type type="char*"/>
				<parameters>
					<parameter name="item" type="GUPnPDIDLLiteItem*"/>
				</parameters>
			</method>
			<method name="set_ref_id" symbol="gupnp_didl_lite_item_set_ref_id">
				<return-type type="void"/>
				<parameters>
					<parameter name="item" type="GUPnPDIDLLiteItem*"/>
					<parameter name="ref_id" type="char*"/>
				</parameters>
			</method>
			<property name="ref-id" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
		</object>
		<object name="GUPnPDIDLLiteObject" parent="GObject" type-name="GUPnPDIDLLiteObject" get-type="gupnp_didl_lite_object_get_type">
			<method name="add_artist" symbol="gupnp_didl_lite_object_add_artist">
				<return-type type="GUPnPDIDLLiteContributor*"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
				</parameters>
			</method>
			<method name="add_author" symbol="gupnp_didl_lite_object_add_author">
				<return-type type="GUPnPDIDLLiteContributor*"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
				</parameters>
			</method>
			<method name="add_creator" symbol="gupnp_didl_lite_object_add_creator">
				<return-type type="GUPnPDIDLLiteContributor*"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
				</parameters>
			</method>
			<method name="add_descriptor" symbol="gupnp_didl_lite_object_add_descriptor">
				<return-type type="GUPnPDIDLLiteDescriptor*"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
				</parameters>
			</method>
			<method name="add_resource" symbol="gupnp_didl_lite_object_add_resource">
				<return-type type="GUPnPDIDLLiteResource*"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
				</parameters>
			</method>
			<method name="get_album" symbol="gupnp_didl_lite_object_get_album">
				<return-type type="char*"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
				</parameters>
			</method>
			<method name="get_album_art" symbol="gupnp_didl_lite_object_get_album_art">
				<return-type type="char*"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
				</parameters>
			</method>
			<method name="get_artist" symbol="gupnp_didl_lite_object_get_artist">
				<return-type type="char*"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
				</parameters>
			</method>
			<method name="get_artists" symbol="gupnp_didl_lite_object_get_artists">
				<return-type type="GList*"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
				</parameters>
			</method>
			<method name="get_author" symbol="gupnp_didl_lite_object_get_author">
				<return-type type="char*"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
				</parameters>
			</method>
			<method name="get_authors" symbol="gupnp_didl_lite_object_get_authors">
				<return-type type="GList*"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
				</parameters>
			</method>
			<method name="get_compat_resource" symbol="gupnp_didl_lite_object_get_compat_resource">
				<return-type type="GUPnPDIDLLiteResource*"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
					<parameter name="sink_protocol_info" type="char*"/>
					<parameter name="lenient" type="gboolean"/>
				</parameters>
			</method>
			<method name="get_creator" symbol="gupnp_didl_lite_object_get_creator">
				<return-type type="char*"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
				</parameters>
			</method>
			<method name="get_creators" symbol="gupnp_didl_lite_object_get_creators">
				<return-type type="GList*"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
				</parameters>
			</method>
			<method name="get_date" symbol="gupnp_didl_lite_object_get_date">
				<return-type type="char*"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
				</parameters>
			</method>
			<method name="get_dc_namespace" symbol="gupnp_didl_lite_object_get_dc_namespace">
				<return-type type="xmlNsPtr"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
				</parameters>
			</method>
			<method name="get_description" symbol="gupnp_didl_lite_object_get_description">
				<return-type type="char*"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
				</parameters>
			</method>
			<method name="get_descriptors" symbol="gupnp_didl_lite_object_get_descriptors">
				<return-type type="GList*"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
				</parameters>
			</method>
			<method name="get_dlna_managed" symbol="gupnp_didl_lite_object_get_dlna_managed">
				<return-type type="GUPnPOCMFlags"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
				</parameters>
			</method>
			<method name="get_dlna_namespace" symbol="gupnp_didl_lite_object_get_dlna_namespace">
				<return-type type="xmlNsPtr"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
				</parameters>
			</method>
			<method name="get_genre" symbol="gupnp_didl_lite_object_get_genre">
				<return-type type="char*"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
				</parameters>
			</method>
			<method name="get_id" symbol="gupnp_didl_lite_object_get_id">
				<return-type type="char*"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
				</parameters>
			</method>
			<method name="get_parent_id" symbol="gupnp_didl_lite_object_get_parent_id">
				<return-type type="char*"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
				</parameters>
			</method>
			<method name="get_properties" symbol="gupnp_didl_lite_object_get_properties">
				<return-type type="GList*"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
					<parameter name="name" type="char*"/>
				</parameters>
			</method>
			<method name="get_resources" symbol="gupnp_didl_lite_object_get_resources">
				<return-type type="GList*"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
				</parameters>
			</method>
			<method name="get_restricted" symbol="gupnp_didl_lite_object_get_restricted">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
				</parameters>
			</method>
			<method name="get_title" symbol="gupnp_didl_lite_object_get_title">
				<return-type type="char*"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
				</parameters>
			</method>
			<method name="get_track_number" symbol="gupnp_didl_lite_object_get_track_number">
				<return-type type="int"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
				</parameters>
			</method>
			<method name="get_upnp_class" symbol="gupnp_didl_lite_object_get_upnp_class">
				<return-type type="char*"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
				</parameters>
			</method>
			<method name="get_upnp_namespace" symbol="gupnp_didl_lite_object_get_upnp_namespace">
				<return-type type="xmlNsPtr"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
				</parameters>
			</method>
			<method name="get_write_status" symbol="gupnp_didl_lite_object_get_write_status">
				<return-type type="char*"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
				</parameters>
			</method>
			<method name="get_xml_node" symbol="gupnp_didl_lite_object_get_xml_node">
				<return-type type="xmlNode*"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
				</parameters>
			</method>
			<method name="set_album" symbol="gupnp_didl_lite_object_set_album">
				<return-type type="void"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
					<parameter name="album" type="char*"/>
				</parameters>
			</method>
			<method name="set_album_art" symbol="gupnp_didl_lite_object_set_album_art">
				<return-type type="void"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
					<parameter name="album_art" type="char*"/>
				</parameters>
			</method>
			<method name="set_artist" symbol="gupnp_didl_lite_object_set_artist">
				<return-type type="void"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
					<parameter name="artist" type="char*"/>
				</parameters>
			</method>
			<method name="set_author" symbol="gupnp_didl_lite_object_set_author">
				<return-type type="void"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
					<parameter name="author" type="char*"/>
				</parameters>
			</method>
			<method name="set_creator" symbol="gupnp_didl_lite_object_set_creator">
				<return-type type="void"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
					<parameter name="creator" type="char*"/>
				</parameters>
			</method>
			<method name="set_date" symbol="gupnp_didl_lite_object_set_date">
				<return-type type="void"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
					<parameter name="date" type="char*"/>
				</parameters>
			</method>
			<method name="set_description" symbol="gupnp_didl_lite_object_set_description">
				<return-type type="void"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
					<parameter name="description" type="char*"/>
				</parameters>
			</method>
			<method name="set_dlna_managed" symbol="gupnp_didl_lite_object_set_dlna_managed">
				<return-type type="void"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
					<parameter name="dlna_managed" type="GUPnPOCMFlags"/>
				</parameters>
			</method>
			<method name="set_genre" symbol="gupnp_didl_lite_object_set_genre">
				<return-type type="void"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
					<parameter name="genre" type="char*"/>
				</parameters>
			</method>
			<method name="set_id" symbol="gupnp_didl_lite_object_set_id">
				<return-type type="void"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
					<parameter name="id" type="char*"/>
				</parameters>
			</method>
			<method name="set_parent_id" symbol="gupnp_didl_lite_object_set_parent_id">
				<return-type type="void"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
					<parameter name="parent_id" type="char*"/>
				</parameters>
			</method>
			<method name="set_restricted" symbol="gupnp_didl_lite_object_set_restricted">
				<return-type type="void"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
					<parameter name="restricted" type="gboolean"/>
				</parameters>
			</method>
			<method name="set_title" symbol="gupnp_didl_lite_object_set_title">
				<return-type type="void"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
					<parameter name="title" type="char*"/>
				</parameters>
			</method>
			<method name="set_track_number" symbol="gupnp_didl_lite_object_set_track_number">
				<return-type type="void"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
					<parameter name="track_number" type="int"/>
				</parameters>
			</method>
			<method name="set_upnp_class" symbol="gupnp_didl_lite_object_set_upnp_class">
				<return-type type="void"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
					<parameter name="upnp_class" type="char*"/>
				</parameters>
			</method>
			<method name="set_write_status" symbol="gupnp_didl_lite_object_set_write_status">
				<return-type type="void"/>
				<parameters>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
					<parameter name="write_status" type="char*"/>
				</parameters>
			</method>
			<property name="album" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="album-art" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="artist" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="author" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="creator" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="date" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="dc-namespace" type="gpointer" readable="1" writable="1" construct="0" construct-only="1"/>
			<property name="description" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="dlna-managed" type="GUPnPOCMFlags" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="dlna-namespace" type="gpointer" readable="1" writable="1" construct="0" construct-only="1"/>
			<property name="genre" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="id" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="parent-id" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="restricted" type="gboolean" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="title" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="track-number" type="gint" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="upnp-class" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="upnp-namespace" type="gpointer" readable="1" writable="1" construct="0" construct-only="1"/>
			<property name="write-status" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="xml-doc" type="GUPnPXMLDoc*" readable="0" writable="1" construct="0" construct-only="1"/>
			<property name="xml-node" type="gpointer" readable="1" writable="1" construct="0" construct-only="1"/>
		</object>
		<object name="GUPnPDIDLLiteParser" parent="GObject" type-name="GUPnPDIDLLiteParser" get-type="gupnp_didl_lite_parser_get_type">
			<constructor name="new" symbol="gupnp_didl_lite_parser_new">
				<return-type type="GUPnPDIDLLiteParser*"/>
			</constructor>
			<method name="parse_didl" symbol="gupnp_didl_lite_parser_parse_didl">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="parser" type="GUPnPDIDLLiteParser*"/>
					<parameter name="didl" type="char*"/>
					<parameter name="error" type="GError**"/>
				</parameters>
			</method>
			<signal name="container-available" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="parser" type="GUPnPDIDLLiteParser*"/>
					<parameter name="container" type="GUPnPDIDLLiteContainer*"/>
				</parameters>
			</signal>
			<signal name="item-available" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="parser" type="GUPnPDIDLLiteParser*"/>
					<parameter name="item" type="GUPnPDIDLLiteItem*"/>
				</parameters>
			</signal>
			<signal name="object-available" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="parser" type="GUPnPDIDLLiteParser*"/>
					<parameter name="object" type="GUPnPDIDLLiteObject*"/>
				</parameters>
			</signal>
			<field name="gupnp_reserved" type="gpointer"/>
		</object>
		<object name="GUPnPDIDLLiteResource" parent="GObject" type-name="GUPnPDIDLLiteResource" get-type="gupnp_didl_lite_resource_get_type">
			<method name="get_audio_channels" symbol="gupnp_didl_lite_resource_get_audio_channels">
				<return-type type="int"/>
				<parameters>
					<parameter name="resource" type="GUPnPDIDLLiteResource*"/>
				</parameters>
			</method>
			<method name="get_bitrate" symbol="gupnp_didl_lite_resource_get_bitrate">
				<return-type type="int"/>
				<parameters>
					<parameter name="resource" type="GUPnPDIDLLiteResource*"/>
				</parameters>
			</method>
			<method name="get_bits_per_sample" symbol="gupnp_didl_lite_resource_get_bits_per_sample">
				<return-type type="int"/>
				<parameters>
					<parameter name="resource" type="GUPnPDIDLLiteResource*"/>
				</parameters>
			</method>
			<method name="get_color_depth" symbol="gupnp_didl_lite_resource_get_color_depth">
				<return-type type="int"/>
				<parameters>
					<parameter name="resource" type="GUPnPDIDLLiteResource*"/>
				</parameters>
			</method>
			<method name="get_duration" symbol="gupnp_didl_lite_resource_get_duration">
				<return-type type="long"/>
				<parameters>
					<parameter name="resource" type="GUPnPDIDLLiteResource*"/>
				</parameters>
			</method>
			<method name="get_height" symbol="gupnp_didl_lite_resource_get_height">
				<return-type type="int"/>
				<parameters>
					<parameter name="resource" type="GUPnPDIDLLiteResource*"/>
				</parameters>
			</method>
			<method name="get_import_uri" symbol="gupnp_didl_lite_resource_get_import_uri">
				<return-type type="char*"/>
				<parameters>
					<parameter name="resource" type="GUPnPDIDLLiteResource*"/>
				</parameters>
			</method>
			<method name="get_protection" symbol="gupnp_didl_lite_resource_get_protection">
				<return-type type="char*"/>
				<parameters>
					<parameter name="resource" type="GUPnPDIDLLiteResource*"/>
				</parameters>
			</method>
			<method name="get_protocol_info" symbol="gupnp_didl_lite_resource_get_protocol_info">
				<return-type type="GUPnPProtocolInfo*"/>
				<parameters>
					<parameter name="resource" type="GUPnPDIDLLiteResource*"/>
				</parameters>
			</method>
			<method name="get_sample_freq" symbol="gupnp_didl_lite_resource_get_sample_freq">
				<return-type type="int"/>
				<parameters>
					<parameter name="resource" type="GUPnPDIDLLiteResource*"/>
				</parameters>
			</method>
			<method name="get_size" symbol="gupnp_didl_lite_resource_get_size">
				<return-type type="long"/>
				<parameters>
					<parameter name="resource" type="GUPnPDIDLLiteResource*"/>
				</parameters>
			</method>
			<method name="get_size64" symbol="gupnp_didl_lite_resource_get_size64">
				<return-type type="gint64"/>
				<parameters>
					<parameter name="resource" type="GUPnPDIDLLiteResource*"/>
				</parameters>
			</method>
			<method name="get_uri" symbol="gupnp_didl_lite_resource_get_uri">
				<return-type type="char*"/>
				<parameters>
					<parameter name="resource" type="GUPnPDIDLLiteResource*"/>
				</parameters>
			</method>
			<method name="get_width" symbol="gupnp_didl_lite_resource_get_width">
				<return-type type="int"/>
				<parameters>
					<parameter name="resource" type="GUPnPDIDLLiteResource*"/>
				</parameters>
			</method>
			<method name="get_xml_node" symbol="gupnp_didl_lite_resource_get_xml_node">
				<return-type type="xmlNode*"/>
				<parameters>
					<parameter name="resource" type="GUPnPDIDLLiteResource*"/>
				</parameters>
			</method>
			<method name="set_audio_channels" symbol="gupnp_didl_lite_resource_set_audio_channels">
				<return-type type="void"/>
				<parameters>
					<parameter name="resource" type="GUPnPDIDLLiteResource*"/>
					<parameter name="n_channels" type="int"/>
				</parameters>
			</method>
			<method name="set_bitrate" symbol="gupnp_didl_lite_resource_set_bitrate">
				<return-type type="void"/>
				<parameters>
					<parameter name="resource" type="GUPnPDIDLLiteResource*"/>
					<parameter name="bitrate" type="int"/>
				</parameters>
			</method>
			<method name="set_bits_per_sample" symbol="gupnp_didl_lite_resource_set_bits_per_sample">
				<return-type type="void"/>
				<parameters>
					<parameter name="resource" type="GUPnPDIDLLiteResource*"/>
					<parameter name="sample_size" type="int"/>
				</parameters>
			</method>
			<method name="set_color_depth" symbol="gupnp_didl_lite_resource_set_color_depth">
				<return-type type="void"/>
				<parameters>
					<parameter name="resource" type="GUPnPDIDLLiteResource*"/>
					<parameter name="color_depth" type="int"/>
				</parameters>
			</method>
			<method name="set_duration" symbol="gupnp_didl_lite_resource_set_duration">
				<return-type type="void"/>
				<parameters>
					<parameter name="resource" type="GUPnPDIDLLiteResource*"/>
					<parameter name="duration" type="long"/>
				</parameters>
			</method>
			<method name="set_height" symbol="gupnp_didl_lite_resource_set_height">
				<return-type type="void"/>
				<parameters>
					<parameter name="resource" type="GUPnPDIDLLiteResource*"/>
					<parameter name="height" type="int"/>
				</parameters>
			</method>
			<method name="set_import_uri" symbol="gupnp_didl_lite_resource_set_import_uri">
				<return-type type="void"/>
				<parameters>
					<parameter name="resource" type="GUPnPDIDLLiteResource*"/>
					<parameter name="import_uri" type="char*"/>
				</parameters>
			</method>
			<method name="set_protection" symbol="gupnp_didl_lite_resource_set_protection">
				<return-type type="void"/>
				<parameters>
					<parameter name="resource" type="GUPnPDIDLLiteResource*"/>
					<parameter name="protection" type="char*"/>
				</parameters>
			</method>
			<method name="set_protocol_info" symbol="gupnp_didl_lite_resource_set_protocol_info">
				<return-type type="void"/>
				<parameters>
					<parameter name="resource" type="GUPnPDIDLLiteResource*"/>
					<parameter name="info" type="GUPnPProtocolInfo*"/>
				</parameters>
			</method>
			<method name="set_sample_freq" symbol="gupnp_didl_lite_resource_set_sample_freq">
				<return-type type="void"/>
				<parameters>
					<parameter name="resource" type="GUPnPDIDLLiteResource*"/>
					<parameter name="sample_freq" type="int"/>
				</parameters>
			</method>
			<method name="set_size" symbol="gupnp_didl_lite_resource_set_size">
				<return-type type="void"/>
				<parameters>
					<parameter name="resource" type="GUPnPDIDLLiteResource*"/>
					<parameter name="size" type="long"/>
				</parameters>
			</method>
			<method name="set_size64" symbol="gupnp_didl_lite_resource_set_size64">
				<return-type type="void"/>
				<parameters>
					<parameter name="resource" type="GUPnPDIDLLiteResource*"/>
					<parameter name="size" type="gint64"/>
				</parameters>
			</method>
			<method name="set_uri" symbol="gupnp_didl_lite_resource_set_uri">
				<return-type type="void"/>
				<parameters>
					<parameter name="resource" type="GUPnPDIDLLiteResource*"/>
					<parameter name="uri" type="char*"/>
				</parameters>
			</method>
			<method name="set_width" symbol="gupnp_didl_lite_resource_set_width">
				<return-type type="void"/>
				<parameters>
					<parameter name="resource" type="GUPnPDIDLLiteResource*"/>
					<parameter name="width" type="int"/>
				</parameters>
			</method>
			<property name="audio-channels" type="gint" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="bitrate" type="gint" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="bits-per-sample" type="gint" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="color-depth" type="gint" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="duration" type="glong" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="height" type="gint" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="import-uri" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="protection" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="protocol-info" type="GUPnPProtocolInfo*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="sample-freq" type="gint" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="size" type="glong" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="size64" type="gint64" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="uri" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="width" type="gint" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="xml-doc" type="GUPnPXMLDoc*" readable="0" writable="1" construct="0" construct-only="1"/>
			<property name="xml-node" type="gpointer" readable="1" writable="1" construct="0" construct-only="1"/>
		</object>
		<object name="GUPnPDIDLLiteWriter" parent="GObject" type-name="GUPnPDIDLLiteWriter" get-type="gupnp_didl_lite_writer_get_type">
			<method name="add_container" symbol="gupnp_didl_lite_writer_add_container">
				<return-type type="GUPnPDIDLLiteContainer*"/>
				<parameters>
					<parameter name="writer" type="GUPnPDIDLLiteWriter*"/>
				</parameters>
			</method>
			<method name="add_descriptor" symbol="gupnp_didl_lite_writer_add_descriptor">
				<return-type type="GUPnPDIDLLiteDescriptor*"/>
				<parameters>
					<parameter name="writer" type="GUPnPDIDLLiteWriter*"/>
				</parameters>
			</method>
			<method name="add_item" symbol="gupnp_didl_lite_writer_add_item">
				<return-type type="GUPnPDIDLLiteItem*"/>
				<parameters>
					<parameter name="writer" type="GUPnPDIDLLiteWriter*"/>
				</parameters>
			</method>
			<method name="filter" symbol="gupnp_didl_lite_writer_filter">
				<return-type type="void"/>
				<parameters>
					<parameter name="writer" type="GUPnPDIDLLiteWriter*"/>
					<parameter name="filter" type="char*"/>
				</parameters>
			</method>
			<method name="get_language" symbol="gupnp_didl_lite_writer_get_language">
				<return-type type="char*"/>
				<parameters>
					<parameter name="writer" type="GUPnPDIDLLiteWriter*"/>
				</parameters>
			</method>
			<method name="get_string" symbol="gupnp_didl_lite_writer_get_string">
				<return-type type="char*"/>
				<parameters>
					<parameter name="writer" type="GUPnPDIDLLiteWriter*"/>
				</parameters>
			</method>
			<method name="get_xml_node" symbol="gupnp_didl_lite_writer_get_xml_node">
				<return-type type="xmlNode*"/>
				<parameters>
					<parameter name="writer" type="GUPnPDIDLLiteWriter*"/>
				</parameters>
			</method>
			<constructor name="new" symbol="gupnp_didl_lite_writer_new">
				<return-type type="GUPnPDIDLLiteWriter*"/>
				<parameters>
					<parameter name="language" type="char*"/>
				</parameters>
			</constructor>
			<property name="language" type="char*" readable="1" writable="1" construct="0" construct-only="1"/>
			<property name="xml-node" type="gpointer" readable="1" writable="0" construct="0" construct-only="0"/>
		</object>
		<object name="GUPnPLastChangeParser" parent="GObject" type-name="GUPnPLastChangeParser" get-type="gupnp_last_change_parser_get_type">
			<constructor name="new" symbol="gupnp_last_change_parser_new">
				<return-type type="GUPnPLastChangeParser*"/>
			</constructor>
			<method name="parse_last_change" symbol="gupnp_last_change_parser_parse_last_change">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="parser" type="GUPnPLastChangeParser*"/>
					<parameter name="instance_id" type="guint"/>
					<parameter name="last_change_xml" type="char*"/>
					<parameter name="error" type="GError**"/>
				</parameters>
			</method>
			<method name="parse_last_change_valist" symbol="gupnp_last_change_parser_parse_last_change_valist">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="parser" type="GUPnPLastChangeParser*"/>
					<parameter name="instance_id" type="guint"/>
					<parameter name="last_change_xml" type="char*"/>
					<parameter name="error" type="GError**"/>
					<parameter name="var_args" type="va_list"/>
				</parameters>
			</method>
		</object>
		<object name="GUPnPProtocolInfo" parent="GObject" type-name="GUPnPProtocolInfo" get-type="gupnp_protocol_info_get_type">
			<method name="get_dlna_conversion" symbol="gupnp_protocol_info_get_dlna_conversion">
				<return-type type="GUPnPDLNAConversion"/>
				<parameters>
					<parameter name="info" type="GUPnPProtocolInfo*"/>
				</parameters>
			</method>
			<method name="get_dlna_flags" symbol="gupnp_protocol_info_get_dlna_flags">
				<return-type type="GUPnPDLNAFlags"/>
				<parameters>
					<parameter name="info" type="GUPnPProtocolInfo*"/>
				</parameters>
			</method>
			<method name="get_dlna_operation" symbol="gupnp_protocol_info_get_dlna_operation">
				<return-type type="GUPnPDLNAOperation"/>
				<parameters>
					<parameter name="info" type="GUPnPProtocolInfo*"/>
				</parameters>
			</method>
			<method name="get_dlna_profile" symbol="gupnp_protocol_info_get_dlna_profile">
				<return-type type="char*"/>
				<parameters>
					<parameter name="info" type="GUPnPProtocolInfo*"/>
				</parameters>
			</method>
			<method name="get_mime_type" symbol="gupnp_protocol_info_get_mime_type">
				<return-type type="char*"/>
				<parameters>
					<parameter name="info" type="GUPnPProtocolInfo*"/>
				</parameters>
			</method>
			<method name="get_network" symbol="gupnp_protocol_info_get_network">
				<return-type type="char*"/>
				<parameters>
					<parameter name="info" type="GUPnPProtocolInfo*"/>
				</parameters>
			</method>
			<method name="get_play_speeds" symbol="gupnp_protocol_info_get_play_speeds">
				<return-type type="char**"/>
				<parameters>
					<parameter name="info" type="GUPnPProtocolInfo*"/>
				</parameters>
			</method>
			<method name="get_protocol" symbol="gupnp_protocol_info_get_protocol">
				<return-type type="char*"/>
				<parameters>
					<parameter name="info" type="GUPnPProtocolInfo*"/>
				</parameters>
			</method>
			<method name="is_compatible" symbol="gupnp_protocol_info_is_compatible">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="info1" type="GUPnPProtocolInfo*"/>
					<parameter name="info2" type="GUPnPProtocolInfo*"/>
				</parameters>
			</method>
			<constructor name="new" symbol="gupnp_protocol_info_new">
				<return-type type="GUPnPProtocolInfo*"/>
			</constructor>
			<constructor name="new_from_string" symbol="gupnp_protocol_info_new_from_string">
				<return-type type="GUPnPProtocolInfo*"/>
				<parameters>
					<parameter name="protocol_info" type="char*"/>
					<parameter name="error" type="GError**"/>
				</parameters>
			</constructor>
			<method name="set_dlna_conversion" symbol="gupnp_protocol_info_set_dlna_conversion">
				<return-type type="void"/>
				<parameters>
					<parameter name="info" type="GUPnPProtocolInfo*"/>
					<parameter name="conversion" type="GUPnPDLNAConversion"/>
				</parameters>
			</method>
			<method name="set_dlna_flags" symbol="gupnp_protocol_info_set_dlna_flags">
				<return-type type="void"/>
				<parameters>
					<parameter name="info" type="GUPnPProtocolInfo*"/>
					<parameter name="flags" type="GUPnPDLNAFlags"/>
				</parameters>
			</method>
			<method name="set_dlna_operation" symbol="gupnp_protocol_info_set_dlna_operation">
				<return-type type="void"/>
				<parameters>
					<parameter name="info" type="GUPnPProtocolInfo*"/>
					<parameter name="operation" type="GUPnPDLNAOperation"/>
				</parameters>
			</method>
			<method name="set_dlna_profile" symbol="gupnp_protocol_info_set_dlna_profile">
				<return-type type="void"/>
				<parameters>
					<parameter name="info" type="GUPnPProtocolInfo*"/>
					<parameter name="profile" type="char*"/>
				</parameters>
			</method>
			<method name="set_mime_type" symbol="gupnp_protocol_info_set_mime_type">
				<return-type type="void"/>
				<parameters>
					<parameter name="info" type="GUPnPProtocolInfo*"/>
					<parameter name="mime_type" type="char*"/>
				</parameters>
			</method>
			<method name="set_network" symbol="gupnp_protocol_info_set_network">
				<return-type type="void"/>
				<parameters>
					<parameter name="info" type="GUPnPProtocolInfo*"/>
					<parameter name="network" type="char*"/>
				</parameters>
			</method>
			<method name="set_play_speeds" symbol="gupnp_protocol_info_set_play_speeds">
				<return-type type="void"/>
				<parameters>
					<parameter name="info" type="GUPnPProtocolInfo*"/>
					<parameter name="speeds" type="char**"/>
				</parameters>
			</method>
			<method name="set_protocol" symbol="gupnp_protocol_info_set_protocol">
				<return-type type="void"/>
				<parameters>
					<parameter name="info" type="GUPnPProtocolInfo*"/>
					<parameter name="protocol" type="char*"/>
				</parameters>
			</method>
			<method name="to_string" symbol="gupnp_protocol_info_to_string">
				<return-type type="char*"/>
				<parameters>
					<parameter name="info" type="GUPnPProtocolInfo*"/>
				</parameters>
			</method>
			<property name="dlna-conversion" type="GUPnPDLNAConversion" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="dlna-flags" type="GUPnPDLNAFlags" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="dlna-operation" type="GUPnPDLNAOperation" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="dlna-profile" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="mime-type" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="network" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="play-speeds" type="GStrv*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="protocol" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
		</object>
		<object name="GUPnPSearchCriteriaParser" parent="GObject" type-name="GUPnPSearchCriteriaParser" get-type="gupnp_search_criteria_parser_get_type">
			<method name="error_quark" symbol="gupnp_search_criteria_parser_error_quark">
				<return-type type="GQuark"/>
			</method>
			<constructor name="new" symbol="gupnp_search_criteria_parser_new">
				<return-type type="GUPnPSearchCriteriaParser*"/>
			</constructor>
			<method name="parse_text" symbol="gupnp_search_criteria_parser_parse_text">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="parser" type="GUPnPSearchCriteriaParser*"/>
					<parameter name="text" type="char*"/>
					<parameter name="error" type="GError**"/>
				</parameters>
			</method>
			<signal name="begin-parens" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="parser" type="GUPnPSearchCriteriaParser*"/>
				</parameters>
			</signal>
			<signal name="conjunction" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="parser" type="GUPnPSearchCriteriaParser*"/>
				</parameters>
			</signal>
			<signal name="disjunction" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="parser" type="GUPnPSearchCriteriaParser*"/>
				</parameters>
			</signal>
			<signal name="end-parens" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="parser" type="GUPnPSearchCriteriaParser*"/>
				</parameters>
			</signal>
			<signal name="expression" when="LAST">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="parser" type="GUPnPSearchCriteriaParser*"/>
					<parameter name="property" type="char*"/>
					<parameter name="op" type="GUPnPSearchCriteriaOp"/>
					<parameter name="value" type="char*"/>
					<parameter name="error" type="gpointer"/>
				</parameters>
			</signal>
		</object>
		<constant name="GUPNP_DIDL_LITE_WRITER_NAMESPACE_DC" type="char*" value="dc"/>
		<constant name="GUPNP_DIDL_LITE_WRITER_NAMESPACE_DLNA" type="char*" value="dlna"/>
		<constant name="GUPNP_DIDL_LITE_WRITER_NAMESPACE_UPNP" type="char*" value="upnp"/>
	</namespace>
</api>
