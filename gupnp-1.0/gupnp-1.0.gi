<?xml version="1.0"?>
<api version="1.0">
	<namespace name="GUPnP">
		<function name="control_error_quark" symbol="gupnp_control_error_quark">
			<return-type type="GQuark"/>
		</function>
		<function name="eventing_error_quark" symbol="gupnp_eventing_error_quark">
			<return-type type="GQuark"/>
		</function>
		<function name="server_error_quark" symbol="gupnp_server_error_quark">
			<return-type type="GQuark"/>
		</function>
		<function name="xml_error_quark" symbol="gupnp_xml_error_quark">
			<return-type type="GQuark"/>
		</function>
		<callback name="GUPnPServiceIntrospectionCallback">
			<return-type type="void"/>
			<parameters>
				<parameter name="info" type="GUPnPServiceInfo*"/>
				<parameter name="introspection" type="GUPnPServiceIntrospection*"/>
				<parameter name="error" type="GError*"/>
				<parameter name="user_data" type="gpointer"/>
			</parameters>
		</callback>
		<callback name="GUPnPServiceProxyActionCallback">
			<return-type type="void"/>
			<parameters>
				<parameter name="proxy" type="GUPnPServiceProxy*"/>
				<parameter name="action" type="GUPnPServiceProxyAction*"/>
				<parameter name="user_data" type="gpointer"/>
			</parameters>
		</callback>
		<callback name="GUPnPServiceProxyNotifyCallback">
			<return-type type="void"/>
			<parameters>
				<parameter name="proxy" type="GUPnPServiceProxy*"/>
				<parameter name="variable" type="char*"/>
				<parameter name="value" type="GValue*"/>
				<parameter name="user_data" type="gpointer"/>
			</parameters>
		</callback>
		<struct name="GUPnPServiceActionArgInfo">
			<field name="name" type="char*"/>
			<field name="direction" type="GUPnPServiceActionArgDirection"/>
			<field name="related_state_variable" type="char*"/>
			<field name="retval" type="gboolean"/>
		</struct>
		<struct name="GUPnPServiceActionInfo">
			<field name="name" type="char*"/>
			<field name="arguments" type="GList*"/>
		</struct>
		<struct name="GUPnPServiceProxyAction">
		</struct>
		<struct name="GUPnPServiceStateVariableInfo">
			<field name="name" type="char*"/>
			<field name="send_events" type="gboolean"/>
			<field name="is_numeric" type="gboolean"/>
			<field name="type" type="GType"/>
			<field name="default_value" type="GValue"/>
			<field name="minimum" type="GValue"/>
			<field name="maximum" type="GValue"/>
			<field name="step" type="GValue"/>
			<field name="allowed_values" type="GList*"/>
		</struct>
		<boxed name="GUPnPBinBase64" type-name="GUPnPBinBase64" get-type="gupnp_bin_base64_get_type">
		</boxed>
		<boxed name="GUPnPBinHex" type-name="GUPnPBinHex" get-type="gupnp_bin_hex_get_type">
		</boxed>
		<boxed name="GUPnPDate" type-name="GUPnPDate" get-type="gupnp_date_get_type">
		</boxed>
		<boxed name="GUPnPDateTime" type-name="GUPnPDateTime" get-type="gupnp_date_time_get_type">
		</boxed>
		<boxed name="GUPnPDateTimeTZ" type-name="GUPnPDateTimeTZ" get-type="gupnp_date_time_tz_get_type">
		</boxed>
		<boxed name="GUPnPServiceAction" type-name="GUPnPServiceAction" get-type="gupnp_service_action_get_type">
			<method name="get" symbol="gupnp_service_action_get">
				<return-type type="void"/>
				<parameters>
					<parameter name="action" type="GUPnPServiceAction*"/>
				</parameters>
			</method>
			<method name="get_argument_count" symbol="gupnp_service_action_get_argument_count">
				<return-type type="guint"/>
				<parameters>
					<parameter name="action" type="GUPnPServiceAction*"/>
				</parameters>
			</method>
			<method name="get_gvalue" symbol="gupnp_service_action_get_gvalue">
				<return-type type="GValue*"/>
				<parameters>
					<parameter name="action" type="GUPnPServiceAction*"/>
					<parameter name="argument" type="char*"/>
					<parameter name="type" type="GType"/>
				</parameters>
			</method>
			<method name="get_locales" symbol="gupnp_service_action_get_locales">
				<return-type type="GList*"/>
				<parameters>
					<parameter name="action" type="GUPnPServiceAction*"/>
				</parameters>
			</method>
			<method name="get_message" symbol="gupnp_service_action_get_message">
				<return-type type="SoupMessage*"/>
				<parameters>
					<parameter name="action" type="GUPnPServiceAction*"/>
				</parameters>
			</method>
			<method name="get_name" symbol="gupnp_service_action_get_name">
				<return-type type="char*"/>
				<parameters>
					<parameter name="action" type="GUPnPServiceAction*"/>
				</parameters>
			</method>
			<method name="get_valist" symbol="gupnp_service_action_get_valist">
				<return-type type="void"/>
				<parameters>
					<parameter name="action" type="GUPnPServiceAction*"/>
					<parameter name="var_args" type="va_list"/>
				</parameters>
			</method>
			<method name="get_value" symbol="gupnp_service_action_get_value">
				<return-type type="void"/>
				<parameters>
					<parameter name="action" type="GUPnPServiceAction*"/>
					<parameter name="argument" type="char*"/>
					<parameter name="value" type="GValue*"/>
				</parameters>
			</method>
			<method name="get_values" symbol="gupnp_service_action_get_values">
				<return-type type="GList*"/>
				<parameters>
					<parameter name="action" type="GUPnPServiceAction*"/>
					<parameter name="arg_names" type="GList*"/>
					<parameter name="arg_types" type="GList*"/>
				</parameters>
			</method>
			<method name="return" symbol="gupnp_service_action_return">
				<return-type type="void"/>
				<parameters>
					<parameter name="action" type="GUPnPServiceAction*"/>
				</parameters>
			</method>
			<method name="return_error" symbol="gupnp_service_action_return_error">
				<return-type type="void"/>
				<parameters>
					<parameter name="action" type="GUPnPServiceAction*"/>
					<parameter name="error_code" type="guint"/>
					<parameter name="error_description" type="char*"/>
				</parameters>
			</method>
			<method name="set" symbol="gupnp_service_action_set">
				<return-type type="void"/>
				<parameters>
					<parameter name="action" type="GUPnPServiceAction*"/>
				</parameters>
			</method>
			<method name="set_valist" symbol="gupnp_service_action_set_valist">
				<return-type type="void"/>
				<parameters>
					<parameter name="action" type="GUPnPServiceAction*"/>
					<parameter name="var_args" type="va_list"/>
				</parameters>
			</method>
			<method name="set_value" symbol="gupnp_service_action_set_value">
				<return-type type="void"/>
				<parameters>
					<parameter name="action" type="GUPnPServiceAction*"/>
					<parameter name="argument" type="char*"/>
					<parameter name="value" type="GValue*"/>
				</parameters>
			</method>
			<method name="set_values" symbol="gupnp_service_action_set_values">
				<return-type type="void"/>
				<parameters>
					<parameter name="action" type="GUPnPServiceAction*"/>
					<parameter name="arg_names" type="GList*"/>
					<parameter name="arg_values" type="GList*"/>
				</parameters>
			</method>
		</boxed>
		<boxed name="GUPnPTime" type-name="GUPnPTime" get-type="gupnp_time_get_type">
		</boxed>
		<boxed name="GUPnPTimeTZ" type-name="GUPnPTimeTZ" get-type="gupnp_time_tz_get_type">
		</boxed>
		<boxed name="GUPnPURI" type-name="GUPnPURI" get-type="gupnp_uri_get_type">
		</boxed>
		<boxed name="GUPnPUUID" type-name="GUPnPUUID" get-type="gupnp_uuid_get_type">
		</boxed>
		<enum name="GUPnPControlError">
			<member name="GUPNP_CONTROL_ERROR_INVALID_ACTION" value="401"/>
			<member name="GUPNP_CONTROL_ERROR_INVALID_ARGS" value="402"/>
			<member name="GUPNP_CONTROL_ERROR_OUT_OF_SYNC" value="403"/>
			<member name="GUPNP_CONTROL_ERROR_ACTION_FAILED" value="501"/>
		</enum>
		<enum name="GUPnPEventingError">
			<member name="GUPNP_EVENTING_ERROR_SUBSCRIPTION_FAILED" value="0"/>
			<member name="GUPNP_EVENTING_ERROR_SUBSCRIPTION_LOST" value="1"/>
			<member name="GUPNP_EVENTING_ERROR_NOTIFY_FAILED" value="2"/>
		</enum>
		<enum name="GUPnPServerError">
			<member name="GUPNP_SERVER_ERROR_INTERNAL_SERVER_ERROR" value="0"/>
			<member name="GUPNP_SERVER_ERROR_NOT_FOUND" value="1"/>
			<member name="GUPNP_SERVER_ERROR_NOT_IMPLEMENTED" value="2"/>
			<member name="GUPNP_SERVER_ERROR_INVALID_RESPONSE" value="3"/>
			<member name="GUPNP_SERVER_ERROR_INVALID_URL" value="4"/>
			<member name="GUPNP_SERVER_ERROR_OTHER" value="5"/>
		</enum>
		<enum name="GUPnPServiceActionArgDirection">
			<member name="GUPNP_SERVICE_ACTION_ARG_DIRECTION_IN" value="0"/>
			<member name="GUPNP_SERVICE_ACTION_ARG_DIRECTION_OUT" value="1"/>
		</enum>
		<enum name="GUPnPXMLError">
			<member name="GUPNP_XML_ERROR_PARSE" value="0"/>
			<member name="GUPNP_XML_ERROR_NO_NODE" value="1"/>
			<member name="GUPNP_XML_ERROR_EMPTY_NODE" value="2"/>
			<member name="GUPNP_XML_ERROR_INVALID_ATTRIBUTE" value="3"/>
			<member name="GUPNP_XML_ERROR_OTHER" value="4"/>
		</enum>
		<object name="GUPnPContext" parent="GSSDPClient" type-name="GUPnPContext" get-type="gupnp_context_get_type">
			<implements>
				<interface name="GInitable"/>
			</implements>
			<method name="get_default_language" symbol="gupnp_context_get_default_language">
				<return-type type="char*"/>
				<parameters>
					<parameter name="context" type="GUPnPContext*"/>
				</parameters>
			</method>
			<method name="get_host_ip" symbol="gupnp_context_get_host_ip">
				<return-type type="char*"/>
				<parameters>
					<parameter name="context" type="GUPnPContext*"/>
				</parameters>
			</method>
			<method name="get_port" symbol="gupnp_context_get_port">
				<return-type type="guint"/>
				<parameters>
					<parameter name="context" type="GUPnPContext*"/>
				</parameters>
			</method>
			<method name="get_server" symbol="gupnp_context_get_server">
				<return-type type="SoupServer*"/>
				<parameters>
					<parameter name="context" type="GUPnPContext*"/>
				</parameters>
			</method>
			<method name="get_session" symbol="gupnp_context_get_session">
				<return-type type="SoupSession*"/>
				<parameters>
					<parameter name="context" type="GUPnPContext*"/>
				</parameters>
			</method>
			<method name="get_subscription_timeout" symbol="gupnp_context_get_subscription_timeout">
				<return-type type="guint"/>
				<parameters>
					<parameter name="context" type="GUPnPContext*"/>
				</parameters>
			</method>
			<method name="host_path" symbol="gupnp_context_host_path">
				<return-type type="void"/>
				<parameters>
					<parameter name="context" type="GUPnPContext*"/>
					<parameter name="local_path" type="char*"/>
					<parameter name="server_path" type="char*"/>
				</parameters>
			</method>
			<method name="host_path_for_agent" symbol="gupnp_context_host_path_for_agent">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="context" type="GUPnPContext*"/>
					<parameter name="local_path" type="char*"/>
					<parameter name="server_path" type="char*"/>
					<parameter name="user_agent" type="GRegex*"/>
				</parameters>
			</method>
			<constructor name="new" symbol="gupnp_context_new">
				<return-type type="GUPnPContext*"/>
				<parameters>
					<parameter name="main_context" type="GMainContext*"/>
					<parameter name="interface" type="char*"/>
					<parameter name="port" type="guint"/>
					<parameter name="error" type="GError**"/>
				</parameters>
			</constructor>
			<method name="set_default_language" symbol="gupnp_context_set_default_language">
				<return-type type="void"/>
				<parameters>
					<parameter name="context" type="GUPnPContext*"/>
					<parameter name="language" type="char*"/>
				</parameters>
			</method>
			<method name="set_subscription_timeout" symbol="gupnp_context_set_subscription_timeout">
				<return-type type="void"/>
				<parameters>
					<parameter name="context" type="GUPnPContext*"/>
					<parameter name="timeout" type="guint"/>
				</parameters>
			</method>
			<method name="unhost_path" symbol="gupnp_context_unhost_path">
				<return-type type="void"/>
				<parameters>
					<parameter name="context" type="GUPnPContext*"/>
					<parameter name="server_path" type="char*"/>
				</parameters>
			</method>
			<property name="default-language" type="char*" readable="1" writable="1" construct="1" construct-only="0"/>
			<property name="port" type="guint" readable="1" writable="1" construct="0" construct-only="1"/>
			<property name="server" type="SoupServer*" readable="1" writable="0" construct="0" construct-only="0"/>
			<property name="session" type="SoupSession*" readable="1" writable="0" construct="0" construct-only="0"/>
			<property name="subscription-timeout" type="guint" readable="1" writable="1" construct="0" construct-only="1"/>
		</object>
		<object name="GUPnPContextManager" parent="GObject" type-name="GUPnPContextManager" get-type="gupnp_context_manager_get_type">
			<method name="create" symbol="gupnp_context_manager_create">
				<return-type type="GUPnPContextManager*"/>
				<parameters>
					<parameter name="port" type="guint"/>
				</parameters>
			</method>
			<method name="manage_control_point" symbol="gupnp_context_manager_manage_control_point">
				<return-type type="void"/>
				<parameters>
					<parameter name="manager" type="GUPnPContextManager*"/>
					<parameter name="control_point" type="GUPnPControlPoint*"/>
				</parameters>
			</method>
			<method name="manage_root_device" symbol="gupnp_context_manager_manage_root_device">
				<return-type type="void"/>
				<parameters>
					<parameter name="manager" type="GUPnPContextManager*"/>
					<parameter name="root_device" type="GUPnPRootDevice*"/>
				</parameters>
			</method>
			<constructor name="new" symbol="gupnp_context_manager_new">
				<return-type type="GUPnPContextManager*"/>
				<parameters>
					<parameter name="main_context" type="GMainContext*"/>
					<parameter name="port" type="guint"/>
				</parameters>
			</constructor>
			<property name="main-context" type="gpointer" readable="1" writable="1" construct="0" construct-only="1"/>
			<property name="port" type="guint" readable="1" writable="1" construct="0" construct-only="1"/>
			<signal name="context-available" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="object" type="GUPnPContextManager*"/>
					<parameter name="p0" type="GUPnPContext*"/>
				</parameters>
			</signal>
			<signal name="context-unavailable" when="FIRST">
				<return-type type="void"/>
				<parameters>
					<parameter name="object" type="GUPnPContextManager*"/>
					<parameter name="p0" type="GUPnPContext*"/>
				</parameters>
			</signal>
		</object>
		<object name="GUPnPControlPoint" parent="GSSDPResourceBrowser" type-name="GUPnPControlPoint" get-type="gupnp_control_point_get_type">
			<method name="get_context" symbol="gupnp_control_point_get_context">
				<return-type type="GUPnPContext*"/>
				<parameters>
					<parameter name="control_point" type="GUPnPControlPoint*"/>
				</parameters>
			</method>
			<method name="get_resource_factory" symbol="gupnp_control_point_get_resource_factory">
				<return-type type="GUPnPResourceFactory*"/>
				<parameters>
					<parameter name="control_point" type="GUPnPControlPoint*"/>
				</parameters>
			</method>
			<method name="list_device_proxies" symbol="gupnp_control_point_list_device_proxies">
				<return-type type="GList*"/>
				<parameters>
					<parameter name="control_point" type="GUPnPControlPoint*"/>
				</parameters>
			</method>
			<method name="list_service_proxies" symbol="gupnp_control_point_list_service_proxies">
				<return-type type="GList*"/>
				<parameters>
					<parameter name="control_point" type="GUPnPControlPoint*"/>
				</parameters>
			</method>
			<constructor name="new" symbol="gupnp_control_point_new">
				<return-type type="GUPnPControlPoint*"/>
				<parameters>
					<parameter name="context" type="GUPnPContext*"/>
					<parameter name="target" type="char*"/>
				</parameters>
			</constructor>
			<constructor name="new_full" symbol="gupnp_control_point_new_full">
				<return-type type="GUPnPControlPoint*"/>
				<parameters>
					<parameter name="context" type="GUPnPContext*"/>
					<parameter name="factory" type="GUPnPResourceFactory*"/>
					<parameter name="target" type="char*"/>
				</parameters>
			</constructor>
			<property name="resource-factory" type="GUPnPResourceFactory*" readable="1" writable="1" construct="0" construct-only="1"/>
			<signal name="device-proxy-available" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="control_point" type="GUPnPControlPoint*"/>
					<parameter name="proxy" type="GUPnPDeviceProxy*"/>
				</parameters>
			</signal>
			<signal name="device-proxy-unavailable" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="control_point" type="GUPnPControlPoint*"/>
					<parameter name="proxy" type="GUPnPDeviceProxy*"/>
				</parameters>
			</signal>
			<signal name="service-proxy-available" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="control_point" type="GUPnPControlPoint*"/>
					<parameter name="proxy" type="GUPnPServiceProxy*"/>
				</parameters>
			</signal>
			<signal name="service-proxy-unavailable" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="control_point" type="GUPnPControlPoint*"/>
					<parameter name="proxy" type="GUPnPServiceProxy*"/>
				</parameters>
			</signal>
		</object>
		<object name="GUPnPDevice" parent="GUPnPDeviceInfo" type-name="GUPnPDevice" get-type="gupnp_device_get_type">
			<property name="root-device" type="GUPnPRootDevice*" readable="1" writable="1" construct="0" construct-only="1"/>
		</object>
		<object name="GUPnPDeviceInfo" parent="GObject" type-name="GUPnPDeviceInfo" get-type="gupnp_device_info_get_type">
			<method name="get_context" symbol="gupnp_device_info_get_context">
				<return-type type="GUPnPContext*"/>
				<parameters>
					<parameter name="info" type="GUPnPDeviceInfo*"/>
				</parameters>
			</method>
			<method name="get_description_value" symbol="gupnp_device_info_get_description_value">
				<return-type type="char*"/>
				<parameters>
					<parameter name="info" type="GUPnPDeviceInfo*"/>
					<parameter name="element" type="char*"/>
				</parameters>
			</method>
			<method name="get_device" symbol="gupnp_device_info_get_device">
				<return-type type="GUPnPDeviceInfo*"/>
				<parameters>
					<parameter name="info" type="GUPnPDeviceInfo*"/>
					<parameter name="type" type="char*"/>
				</parameters>
			</method>
			<method name="get_device_type" symbol="gupnp_device_info_get_device_type">
				<return-type type="char*"/>
				<parameters>
					<parameter name="info" type="GUPnPDeviceInfo*"/>
				</parameters>
			</method>
			<method name="get_friendly_name" symbol="gupnp_device_info_get_friendly_name">
				<return-type type="char*"/>
				<parameters>
					<parameter name="info" type="GUPnPDeviceInfo*"/>
				</parameters>
			</method>
			<method name="get_icon_url" symbol="gupnp_device_info_get_icon_url">
				<return-type type="char*"/>
				<parameters>
					<parameter name="info" type="GUPnPDeviceInfo*"/>
					<parameter name="requested_mime_type" type="char*"/>
					<parameter name="requested_depth" type="int"/>
					<parameter name="requested_width" type="int"/>
					<parameter name="requested_height" type="int"/>
					<parameter name="prefer_bigger" type="gboolean"/>
					<parameter name="mime_type" type="char**"/>
					<parameter name="depth" type="int*"/>
					<parameter name="width" type="int*"/>
					<parameter name="height" type="int*"/>
				</parameters>
			</method>
			<method name="get_location" symbol="gupnp_device_info_get_location">
				<return-type type="char*"/>
				<parameters>
					<parameter name="info" type="GUPnPDeviceInfo*"/>
				</parameters>
			</method>
			<method name="get_manufacturer" symbol="gupnp_device_info_get_manufacturer">
				<return-type type="char*"/>
				<parameters>
					<parameter name="info" type="GUPnPDeviceInfo*"/>
				</parameters>
			</method>
			<method name="get_manufacturer_url" symbol="gupnp_device_info_get_manufacturer_url">
				<return-type type="char*"/>
				<parameters>
					<parameter name="info" type="GUPnPDeviceInfo*"/>
				</parameters>
			</method>
			<method name="get_model_description" symbol="gupnp_device_info_get_model_description">
				<return-type type="char*"/>
				<parameters>
					<parameter name="info" type="GUPnPDeviceInfo*"/>
				</parameters>
			</method>
			<method name="get_model_name" symbol="gupnp_device_info_get_model_name">
				<return-type type="char*"/>
				<parameters>
					<parameter name="info" type="GUPnPDeviceInfo*"/>
				</parameters>
			</method>
			<method name="get_model_number" symbol="gupnp_device_info_get_model_number">
				<return-type type="char*"/>
				<parameters>
					<parameter name="info" type="GUPnPDeviceInfo*"/>
				</parameters>
			</method>
			<method name="get_model_url" symbol="gupnp_device_info_get_model_url">
				<return-type type="char*"/>
				<parameters>
					<parameter name="info" type="GUPnPDeviceInfo*"/>
				</parameters>
			</method>
			<method name="get_presentation_url" symbol="gupnp_device_info_get_presentation_url">
				<return-type type="char*"/>
				<parameters>
					<parameter name="info" type="GUPnPDeviceInfo*"/>
				</parameters>
			</method>
			<method name="get_resource_factory" symbol="gupnp_device_info_get_resource_factory">
				<return-type type="GUPnPResourceFactory*"/>
				<parameters>
					<parameter name="device_info" type="GUPnPDeviceInfo*"/>
				</parameters>
			</method>
			<method name="get_serial_number" symbol="gupnp_device_info_get_serial_number">
				<return-type type="char*"/>
				<parameters>
					<parameter name="info" type="GUPnPDeviceInfo*"/>
				</parameters>
			</method>
			<method name="get_service" symbol="gupnp_device_info_get_service">
				<return-type type="GUPnPServiceInfo*"/>
				<parameters>
					<parameter name="info" type="GUPnPDeviceInfo*"/>
					<parameter name="type" type="char*"/>
				</parameters>
			</method>
			<method name="get_udn" symbol="gupnp_device_info_get_udn">
				<return-type type="char*"/>
				<parameters>
					<parameter name="info" type="GUPnPDeviceInfo*"/>
				</parameters>
			</method>
			<method name="get_upc" symbol="gupnp_device_info_get_upc">
				<return-type type="char*"/>
				<parameters>
					<parameter name="info" type="GUPnPDeviceInfo*"/>
				</parameters>
			</method>
			<method name="get_url_base" symbol="gupnp_device_info_get_url_base">
				<return-type type="SoupURI*"/>
				<parameters>
					<parameter name="info" type="GUPnPDeviceInfo*"/>
				</parameters>
			</method>
			<method name="list_device_types" symbol="gupnp_device_info_list_device_types">
				<return-type type="GList*"/>
				<parameters>
					<parameter name="info" type="GUPnPDeviceInfo*"/>
				</parameters>
			</method>
			<method name="list_devices" symbol="gupnp_device_info_list_devices">
				<return-type type="GList*"/>
				<parameters>
					<parameter name="info" type="GUPnPDeviceInfo*"/>
				</parameters>
			</method>
			<method name="list_dlna_capabilities" symbol="gupnp_device_info_list_dlna_capabilities">
				<return-type type="GList*"/>
				<parameters>
					<parameter name="info" type="GUPnPDeviceInfo*"/>
				</parameters>
			</method>
			<method name="list_service_types" symbol="gupnp_device_info_list_service_types">
				<return-type type="GList*"/>
				<parameters>
					<parameter name="info" type="GUPnPDeviceInfo*"/>
				</parameters>
			</method>
			<method name="list_services" symbol="gupnp_device_info_list_services">
				<return-type type="GList*"/>
				<parameters>
					<parameter name="info" type="GUPnPDeviceInfo*"/>
				</parameters>
			</method>
			<property name="context" type="GUPnPContext*" readable="1" writable="1" construct="0" construct-only="1"/>
			<property name="device-type" type="char*" readable="1" writable="1" construct="0" construct-only="1"/>
			<property name="document" type="GUPnPXMLDoc*" readable="0" writable="1" construct="0" construct-only="1"/>
			<property name="element" type="gpointer" readable="0" writable="1" construct="0" construct-only="1"/>
			<property name="location" type="char*" readable="1" writable="1" construct="0" construct-only="1"/>
			<property name="resource-factory" type="GUPnPResourceFactory*" readable="1" writable="1" construct="0" construct-only="1"/>
			<property name="udn" type="char*" readable="1" writable="1" construct="0" construct-only="1"/>
			<property name="url-base" type="gpointer" readable="1" writable="1" construct="0" construct-only="1"/>
			<vfunc name="get_device">
				<return-type type="GUPnPDeviceInfo*"/>
				<parameters>
					<parameter name="info" type="GUPnPDeviceInfo*"/>
					<parameter name="element" type="xmlNode*"/>
				</parameters>
			</vfunc>
			<vfunc name="get_element">
				<return-type type="xmlNode*"/>
				<parameters>
					<parameter name="info" type="GUPnPDeviceInfo*"/>
				</parameters>
			</vfunc>
			<vfunc name="get_service">
				<return-type type="GUPnPServiceInfo*"/>
				<parameters>
					<parameter name="info" type="GUPnPDeviceInfo*"/>
					<parameter name="element" type="xmlNode*"/>
				</parameters>
			</vfunc>
		</object>
		<object name="GUPnPDeviceProxy" parent="GUPnPDeviceInfo" type-name="GUPnPDeviceProxy" get-type="gupnp_device_proxy_get_type">
		</object>
		<object name="GUPnPResourceFactory" parent="GObject" type-name="GUPnPResourceFactory" get-type="gupnp_resource_factory_get_type">
			<method name="get_default" symbol="gupnp_resource_factory_get_default">
				<return-type type="GUPnPResourceFactory*"/>
			</method>
			<constructor name="new" symbol="gupnp_resource_factory_new">
				<return-type type="GUPnPResourceFactory*"/>
			</constructor>
			<method name="register_resource_proxy_type" symbol="gupnp_resource_factory_register_resource_proxy_type">
				<return-type type="void"/>
				<parameters>
					<parameter name="factory" type="GUPnPResourceFactory*"/>
					<parameter name="upnp_type" type="char*"/>
					<parameter name="type" type="GType"/>
				</parameters>
			</method>
			<method name="register_resource_type" symbol="gupnp_resource_factory_register_resource_type">
				<return-type type="void"/>
				<parameters>
					<parameter name="factory" type="GUPnPResourceFactory*"/>
					<parameter name="upnp_type" type="char*"/>
					<parameter name="type" type="GType"/>
				</parameters>
			</method>
			<method name="unregister_resource_proxy_type" symbol="gupnp_resource_factory_unregister_resource_proxy_type">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="factory" type="GUPnPResourceFactory*"/>
					<parameter name="upnp_type" type="char*"/>
				</parameters>
			</method>
			<method name="unregister_resource_type" symbol="gupnp_resource_factory_unregister_resource_type">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="factory" type="GUPnPResourceFactory*"/>
					<parameter name="upnp_type" type="char*"/>
				</parameters>
			</method>
		</object>
		<object name="GUPnPRootDevice" parent="GUPnPDevice" type-name="GUPnPRootDevice" get-type="gupnp_root_device_get_type">
			<method name="get_available" symbol="gupnp_root_device_get_available">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="root_device" type="GUPnPRootDevice*"/>
				</parameters>
			</method>
			<method name="get_description_dir" symbol="gupnp_root_device_get_description_dir">
				<return-type type="char*"/>
				<parameters>
					<parameter name="root_device" type="GUPnPRootDevice*"/>
				</parameters>
			</method>
			<method name="get_description_path" symbol="gupnp_root_device_get_description_path">
				<return-type type="char*"/>
				<parameters>
					<parameter name="root_device" type="GUPnPRootDevice*"/>
				</parameters>
			</method>
			<method name="get_relative_location" symbol="gupnp_root_device_get_relative_location">
				<return-type type="char*"/>
				<parameters>
					<parameter name="root_device" type="GUPnPRootDevice*"/>
				</parameters>
			</method>
			<constructor name="new" symbol="gupnp_root_device_new">
				<return-type type="GUPnPRootDevice*"/>
				<parameters>
					<parameter name="context" type="GUPnPContext*"/>
					<parameter name="description_path" type="char*"/>
					<parameter name="description_dir" type="char*"/>
				</parameters>
			</constructor>
			<constructor name="new_full" symbol="gupnp_root_device_new_full">
				<return-type type="GUPnPRootDevice*"/>
				<parameters>
					<parameter name="context" type="GUPnPContext*"/>
					<parameter name="factory" type="GUPnPResourceFactory*"/>
					<parameter name="description_doc" type="GUPnPXMLDoc*"/>
					<parameter name="description_path" type="char*"/>
					<parameter name="description_dir" type="char*"/>
				</parameters>
			</constructor>
			<method name="set_available" symbol="gupnp_root_device_set_available">
				<return-type type="void"/>
				<parameters>
					<parameter name="root_device" type="GUPnPRootDevice*"/>
					<parameter name="available" type="gboolean"/>
				</parameters>
			</method>
			<property name="available" type="gboolean" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="description-dir" type="char*" readable="1" writable="1" construct="0" construct-only="1"/>
			<property name="description-doc" type="GUPnPXMLDoc*" readable="0" writable="1" construct="0" construct-only="1"/>
			<property name="description-path" type="char*" readable="1" writable="1" construct="0" construct-only="1"/>
		</object>
		<object name="GUPnPService" parent="GUPnPServiceInfo" type-name="GUPnPService" get-type="gupnp_service_get_type">
			<method name="freeze_notify" symbol="gupnp_service_freeze_notify">
				<return-type type="void"/>
				<parameters>
					<parameter name="service" type="GUPnPService*"/>
				</parameters>
			</method>
			<method name="notify" symbol="gupnp_service_notify">
				<return-type type="void"/>
				<parameters>
					<parameter name="service" type="GUPnPService*"/>
				</parameters>
			</method>
			<method name="notify_valist" symbol="gupnp_service_notify_valist">
				<return-type type="void"/>
				<parameters>
					<parameter name="service" type="GUPnPService*"/>
					<parameter name="var_args" type="va_list"/>
				</parameters>
			</method>
			<method name="notify_value" symbol="gupnp_service_notify_value">
				<return-type type="void"/>
				<parameters>
					<parameter name="service" type="GUPnPService*"/>
					<parameter name="variable" type="char*"/>
					<parameter name="value" type="GValue*"/>
				</parameters>
			</method>
			<method name="signals_autoconnect" symbol="gupnp_service_signals_autoconnect">
				<return-type type="void"/>
				<parameters>
					<parameter name="service" type="GUPnPService*"/>
					<parameter name="user_data" type="gpointer"/>
					<parameter name="error" type="GError**"/>
				</parameters>
			</method>
			<method name="thaw_notify" symbol="gupnp_service_thaw_notify">
				<return-type type="void"/>
				<parameters>
					<parameter name="service" type="GUPnPService*"/>
				</parameters>
			</method>
			<property name="root-device" type="GUPnPRootDevice*" readable="1" writable="1" construct="0" construct-only="1"/>
			<signal name="action-invoked" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="service" type="GUPnPService*"/>
					<parameter name="action" type="GUPnPServiceAction*"/>
				</parameters>
			</signal>
			<signal name="notify-failed" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="service" type="GUPnPService*"/>
					<parameter name="callback_urls" type="gpointer"/>
					<parameter name="reason" type="gpointer"/>
				</parameters>
			</signal>
			<signal name="query-variable" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="service" type="GUPnPService*"/>
					<parameter name="variable" type="char*"/>
					<parameter name="value" type="gpointer"/>
				</parameters>
			</signal>
		</object>
		<object name="GUPnPServiceInfo" parent="GObject" type-name="GUPnPServiceInfo" get-type="gupnp_service_info_get_type">
			<method name="get_context" symbol="gupnp_service_info_get_context">
				<return-type type="GUPnPContext*"/>
				<parameters>
					<parameter name="info" type="GUPnPServiceInfo*"/>
				</parameters>
			</method>
			<method name="get_control_url" symbol="gupnp_service_info_get_control_url">
				<return-type type="char*"/>
				<parameters>
					<parameter name="info" type="GUPnPServiceInfo*"/>
				</parameters>
			</method>
			<method name="get_event_subscription_url" symbol="gupnp_service_info_get_event_subscription_url">
				<return-type type="char*"/>
				<parameters>
					<parameter name="info" type="GUPnPServiceInfo*"/>
				</parameters>
			</method>
			<method name="get_id" symbol="gupnp_service_info_get_id">
				<return-type type="char*"/>
				<parameters>
					<parameter name="info" type="GUPnPServiceInfo*"/>
				</parameters>
			</method>
			<method name="get_introspection" symbol="gupnp_service_info_get_introspection">
				<return-type type="GUPnPServiceIntrospection*"/>
				<parameters>
					<parameter name="info" type="GUPnPServiceInfo*"/>
					<parameter name="error" type="GError**"/>
				</parameters>
			</method>
			<method name="get_introspection_async" symbol="gupnp_service_info_get_introspection_async">
				<return-type type="void"/>
				<parameters>
					<parameter name="info" type="GUPnPServiceInfo*"/>
					<parameter name="callback" type="GUPnPServiceIntrospectionCallback"/>
					<parameter name="user_data" type="gpointer"/>
				</parameters>
			</method>
			<method name="get_location" symbol="gupnp_service_info_get_location">
				<return-type type="char*"/>
				<parameters>
					<parameter name="info" type="GUPnPServiceInfo*"/>
				</parameters>
			</method>
			<method name="get_scpd_url" symbol="gupnp_service_info_get_scpd_url">
				<return-type type="char*"/>
				<parameters>
					<parameter name="info" type="GUPnPServiceInfo*"/>
				</parameters>
			</method>
			<method name="get_service_type" symbol="gupnp_service_info_get_service_type">
				<return-type type="char*"/>
				<parameters>
					<parameter name="info" type="GUPnPServiceInfo*"/>
				</parameters>
			</method>
			<method name="get_udn" symbol="gupnp_service_info_get_udn">
				<return-type type="char*"/>
				<parameters>
					<parameter name="info" type="GUPnPServiceInfo*"/>
				</parameters>
			</method>
			<method name="get_url_base" symbol="gupnp_service_info_get_url_base">
				<return-type type="SoupURI*"/>
				<parameters>
					<parameter name="info" type="GUPnPServiceInfo*"/>
				</parameters>
			</method>
			<property name="context" type="GUPnPContext*" readable="1" writable="1" construct="0" construct-only="1"/>
			<property name="document" type="GUPnPXMLDoc*" readable="0" writable="1" construct="0" construct-only="1"/>
			<property name="element" type="gpointer" readable="0" writable="1" construct="0" construct-only="1"/>
			<property name="location" type="char*" readable="1" writable="1" construct="0" construct-only="1"/>
			<property name="service-type" type="char*" readable="1" writable="1" construct="0" construct-only="1"/>
			<property name="udn" type="char*" readable="1" writable="1" construct="0" construct-only="1"/>
			<property name="url-base" type="gpointer" readable="1" writable="1" construct="0" construct-only="1"/>
		</object>
		<object name="GUPnPServiceIntrospection" parent="GObject" type-name="GUPnPServiceIntrospection" get-type="gupnp_service_introspection_get_type">
			<method name="get_action" symbol="gupnp_service_introspection_get_action">
				<return-type type="GUPnPServiceActionInfo*"/>
				<parameters>
					<parameter name="introspection" type="GUPnPServiceIntrospection*"/>
					<parameter name="action_name" type="gchar*"/>
				</parameters>
			</method>
			<method name="get_state_variable" symbol="gupnp_service_introspection_get_state_variable">
				<return-type type="GUPnPServiceStateVariableInfo*"/>
				<parameters>
					<parameter name="introspection" type="GUPnPServiceIntrospection*"/>
					<parameter name="variable_name" type="gchar*"/>
				</parameters>
			</method>
			<method name="list_action_names" symbol="gupnp_service_introspection_list_action_names">
				<return-type type="GList*"/>
				<parameters>
					<parameter name="introspection" type="GUPnPServiceIntrospection*"/>
				</parameters>
			</method>
			<method name="list_actions" symbol="gupnp_service_introspection_list_actions">
				<return-type type="GList*"/>
				<parameters>
					<parameter name="introspection" type="GUPnPServiceIntrospection*"/>
				</parameters>
			</method>
			<method name="list_state_variable_names" symbol="gupnp_service_introspection_list_state_variable_names">
				<return-type type="GList*"/>
				<parameters>
					<parameter name="introspection" type="GUPnPServiceIntrospection*"/>
				</parameters>
			</method>
			<method name="list_state_variables" symbol="gupnp_service_introspection_list_state_variables">
				<return-type type="GList*"/>
				<parameters>
					<parameter name="introspection" type="GUPnPServiceIntrospection*"/>
				</parameters>
			</method>
			<property name="scpd" type="gpointer" readable="0" writable="1" construct="0" construct-only="1"/>
		</object>
		<object name="GUPnPServiceProxy" parent="GUPnPServiceInfo" type-name="GUPnPServiceProxy" get-type="gupnp_service_proxy_get_type">
			<method name="add_notify" symbol="gupnp_service_proxy_add_notify">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="proxy" type="GUPnPServiceProxy*"/>
					<parameter name="variable" type="char*"/>
					<parameter name="type" type="GType"/>
					<parameter name="callback" type="GUPnPServiceProxyNotifyCallback"/>
					<parameter name="user_data" type="gpointer"/>
				</parameters>
			</method>
			<method name="begin_action" symbol="gupnp_service_proxy_begin_action">
				<return-type type="GUPnPServiceProxyAction*"/>
				<parameters>
					<parameter name="proxy" type="GUPnPServiceProxy*"/>
					<parameter name="action" type="char*"/>
					<parameter name="callback" type="GUPnPServiceProxyActionCallback"/>
					<parameter name="user_data" type="gpointer"/>
				</parameters>
			</method>
			<method name="begin_action_hash" symbol="gupnp_service_proxy_begin_action_hash">
				<return-type type="GUPnPServiceProxyAction*"/>
				<parameters>
					<parameter name="proxy" type="GUPnPServiceProxy*"/>
					<parameter name="action" type="char*"/>
					<parameter name="callback" type="GUPnPServiceProxyActionCallback"/>
					<parameter name="user_data" type="gpointer"/>
					<parameter name="hash" type="GHashTable*"/>
				</parameters>
			</method>
			<method name="begin_action_list" symbol="gupnp_service_proxy_begin_action_list">
				<return-type type="GUPnPServiceProxyAction*"/>
				<parameters>
					<parameter name="proxy" type="GUPnPServiceProxy*"/>
					<parameter name="action" type="char*"/>
					<parameter name="in_names" type="GList*"/>
					<parameter name="in_values" type="GList*"/>
					<parameter name="callback" type="GUPnPServiceProxyActionCallback"/>
					<parameter name="user_data" type="gpointer"/>
				</parameters>
			</method>
			<method name="begin_action_valist" symbol="gupnp_service_proxy_begin_action_valist">
				<return-type type="GUPnPServiceProxyAction*"/>
				<parameters>
					<parameter name="proxy" type="GUPnPServiceProxy*"/>
					<parameter name="action" type="char*"/>
					<parameter name="callback" type="GUPnPServiceProxyActionCallback"/>
					<parameter name="user_data" type="gpointer"/>
					<parameter name="var_args" type="va_list"/>
				</parameters>
			</method>
			<method name="cancel_action" symbol="gupnp_service_proxy_cancel_action">
				<return-type type="void"/>
				<parameters>
					<parameter name="proxy" type="GUPnPServiceProxy*"/>
					<parameter name="action" type="GUPnPServiceProxyAction*"/>
				</parameters>
			</method>
			<method name="end_action" symbol="gupnp_service_proxy_end_action">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="proxy" type="GUPnPServiceProxy*"/>
					<parameter name="action" type="GUPnPServiceProxyAction*"/>
					<parameter name="error" type="GError**"/>
				</parameters>
			</method>
			<method name="end_action_hash" symbol="gupnp_service_proxy_end_action_hash">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="proxy" type="GUPnPServiceProxy*"/>
					<parameter name="action" type="GUPnPServiceProxyAction*"/>
					<parameter name="error" type="GError**"/>
					<parameter name="hash" type="GHashTable*"/>
				</parameters>
			</method>
			<method name="end_action_list" symbol="gupnp_service_proxy_end_action_list">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="proxy" type="GUPnPServiceProxy*"/>
					<parameter name="action" type="GUPnPServiceProxyAction*"/>
					<parameter name="error" type="GError**"/>
					<parameter name="out_names" type="GList*"/>
					<parameter name="out_types" type="GList*"/>
					<parameter name="out_values" type="GList**"/>
				</parameters>
			</method>
			<method name="end_action_valist" symbol="gupnp_service_proxy_end_action_valist">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="proxy" type="GUPnPServiceProxy*"/>
					<parameter name="action" type="GUPnPServiceProxyAction*"/>
					<parameter name="error" type="GError**"/>
					<parameter name="var_args" type="va_list"/>
				</parameters>
			</method>
			<method name="get_subscribed" symbol="gupnp_service_proxy_get_subscribed">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="proxy" type="GUPnPServiceProxy*"/>
				</parameters>
			</method>
			<method name="remove_notify" symbol="gupnp_service_proxy_remove_notify">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="proxy" type="GUPnPServiceProxy*"/>
					<parameter name="variable" type="char*"/>
					<parameter name="callback" type="GUPnPServiceProxyNotifyCallback"/>
					<parameter name="user_data" type="gpointer"/>
				</parameters>
			</method>
			<method name="send_action" symbol="gupnp_service_proxy_send_action">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="proxy" type="GUPnPServiceProxy*"/>
					<parameter name="action" type="char*"/>
					<parameter name="error" type="GError**"/>
				</parameters>
			</method>
			<method name="send_action_hash" symbol="gupnp_service_proxy_send_action_hash">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="proxy" type="GUPnPServiceProxy*"/>
					<parameter name="action" type="char*"/>
					<parameter name="error" type="GError**"/>
					<parameter name="in_hash" type="GHashTable*"/>
					<parameter name="out_hash" type="GHashTable*"/>
				</parameters>
			</method>
			<method name="send_action_list" symbol="gupnp_service_proxy_send_action_list">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="proxy" type="GUPnPServiceProxy*"/>
					<parameter name="action" type="char*"/>
					<parameter name="error" type="GError**"/>
					<parameter name="in_names" type="GList*"/>
					<parameter name="in_values" type="GList*"/>
					<parameter name="out_names" type="GList*"/>
					<parameter name="out_types" type="GList*"/>
					<parameter name="out_values" type="GList**"/>
				</parameters>
			</method>
			<method name="send_action_valist" symbol="gupnp_service_proxy_send_action_valist">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="proxy" type="GUPnPServiceProxy*"/>
					<parameter name="action" type="char*"/>
					<parameter name="error" type="GError**"/>
					<parameter name="var_args" type="va_list"/>
				</parameters>
			</method>
			<method name="set_subscribed" symbol="gupnp_service_proxy_set_subscribed">
				<return-type type="void"/>
				<parameters>
					<parameter name="proxy" type="GUPnPServiceProxy*"/>
					<parameter name="subscribed" type="gboolean"/>
				</parameters>
			</method>
			<property name="subscribed" type="gboolean" readable="1" writable="1" construct="0" construct-only="0"/>
			<signal name="subscription-lost" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="proxy" type="GUPnPServiceProxy*"/>
					<parameter name="reason" type="gpointer"/>
				</parameters>
			</signal>
		</object>
		<object name="GUPnPXMLDoc" parent="GObject" type-name="GUPnPXMLDoc" get-type="gupnp_xml_doc_get_type">
			<constructor name="new" symbol="gupnp_xml_doc_new">
				<return-type type="GUPnPXMLDoc*"/>
				<parameters>
					<parameter name="xml_doc" type="xmlDoc*"/>
				</parameters>
			</constructor>
			<constructor name="new_from_path" symbol="gupnp_xml_doc_new_from_path">
				<return-type type="GUPnPXMLDoc*"/>
				<parameters>
					<parameter name="path" type="char*"/>
					<parameter name="error" type="GError**"/>
				</parameters>
			</constructor>
			<field name="doc" type="xmlDoc*"/>
		</object>
	</namespace>
</api>
