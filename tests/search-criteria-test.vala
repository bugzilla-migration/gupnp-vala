/*
 * Copyright (C) 2008 OpenedHand Ltd.
 * Copyright (C) 2008,2010 Zeeshan Ali (Khattak) <zeeshanak@gnome.org>.
 *
 * Author: Jussi Kukkonen <jku@openedhand.com>
 *         Zeeshan Ali (Khattak) <zeeshanak@gnome.org>
 *
 * This file is part of gupnp-vala.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

using GLib;
using GUPnP;

/**
 *  Usage:
 *    ./search-criteria-test "<search string>"
 *  Example
 ./search-criteria-test \
 "upnp:class = \"object.container.person.musicArtist\" \
 and (@refID exists false or dc:title contains 'foo')"
 */

public class Test.SearchCriteriaTest : Object {

    public static int main (string[] args) {
        if (args.length != 2) {
            print ("Usage:\n");
            print ("\t%s \"<search string>\"\n", args[0]);
            print ("Example:\n");
            print ("\t%s \"dc:title contains 'foo'\"\n", args[0]);
            return 1;
        }
        SearchCriteriaTest test = new SearchCriteriaTest ();
        return test.parse (args[1]);
    }

    private int parse (string str) {

        var parser = new SearchCriteriaParser ();
        parser.expression.connect (on_expression);
        parser.conjunction.connect (on_conjunction);
        parser.disjunction.connect (on_disjunction);
        parser.begin_parens.connect (on_begin_parens);
        parser.end_parens.connect (on_end_parens);

        try {
            parser.parse_text (str);
        } catch (Error err) {
            printerr ("Parse error:%s\n", err.message);
        }
        print ("\n");
        return 0;
    }

    private bool on_expression (SearchCriteriaParser parser,
                                string               property,
                                SearchCriteriaOp     op,
                                string               value,
                                void                *error) {
        print ("%s OP%u %s", property, op, value);
        return true;
    }

    private void on_conjunction (SearchCriteriaParser parser) {
        print (" AND ");
    }

    private void on_disjunction (SearchCriteriaParser parser) {
        print (" OR ");
    }

    private void on_begin_parens (SearchCriteriaParser parser) {
        print ("(");
    }

    private void on_end_parens (SearchCriteriaParser parser) {
        print (")");
    }
}
