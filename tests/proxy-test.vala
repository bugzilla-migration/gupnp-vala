/*
 * Copyright (C) 2008 OpenedHand Ltd.
 * Copyright (C) 2008,2010 Zeeshan Ali (Khattak) <zeeshanak@gnome.org>.
 *
 * Author: Jussi Kukkonen <jku@openedhand.com>
 *         Zeeshan Ali (Khattak) <zeeshanak@gnome.org>
 *
 * This file is part of gupnp-vala.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

using GLib;
using GUPnP;

/*
 * TODO:
 *  * call setlocale
 *  * SIGTERM handler?
 */

public class Test.ProxyTest : Object {

    public static int main (string[] args) {
        Context ctxt;

        try {
            ctxt = new Context (null, null, 0);
        } catch (Error err) {
            critical (err.message);
            return 1;
        }

        ControlPoint cp = new ControlPoint
                (ctxt, "urn:schemas-upnp-org:service:ContentDirectory:1");
        cp.service_proxy_available.connect (on_service_proxy_available);
        cp.service_proxy_unavailable.connect (on_service_proxy_unavailable);
        cp.active = true;

        MainLoop loop = new MainLoop (null, false);
        loop.run();

        return 0;
    }

    private static void on_service_proxy_available (ControlPoint cp,
                                                    ServiceProxy proxy) {
        print ("ContentDirectory available:\n");
        print ("\tlocation: %s\n", proxy.location);

        /* We want to be notified whenever SystemUpdateID changes */
        proxy.add_notify ("SystemUpdateID", typeof (uint), on_notify);

        /* subscribe */
        proxy.subscription_lost.connect (on_subscription_lost);
        proxy.subscribed = true;

        /* test action IO */
        try {
            string result;
            uint count, total;

            proxy.send_action
                ("Browse",
                 /* IN args */
                 "ObjectID", typeof (string), "0",
                 "BrowseFlag", typeof (string), "BrowseDirectChildren",
                 "Filter", typeof (string), "*",
                 "StartingIndex", typeof (uint), 0,
                 "RequestedCount", typeof (uint), 0,
                 "SortCriteria", typeof (string), "",
                 null,
                 /* OUT args */
                 "Result", typeof (string), out result,
                 "NumberReturned", typeof (uint), out count,
                 "TotalMatches", typeof (uint), out total,
                 null);
            print ("Browse returned:\n");
            print ("\tResult:         %s\n", result);
            print ("\tNumberReturned: %u\n", count);
            print ("\tTotalMatches:   %u\n", total);
        } catch (Error err) {
            printerr ("Error: %s\n", err.message);
        }
    }

    private static void on_service_proxy_unavailable (ControlPoint cp,
                                                      ServiceProxy proxy) {
        print ("ContentDirectory unavailable:\n");
        print ("\tlocation: %s\n", proxy.location);
    }

    private static void on_notify (ServiceProxy proxy,
                                   string       variable,
                                   Value        value) {
        print ("Received a notification for variable '%s':\n",
                variable);
        print ("\tvalue:     %u\n", value.get_uint ());
    }

    private static void on_subscription_lost (ServiceProxy proxy,
                                              Error        reason) {
        print ("Lost subscription: %s\n", reason.message);

    }
}
