/*
 * Copyright (C) 2008 OpenedHand Ltd.
 * Copyright (C) 2008,2010 Zeeshan Ali (Khattak) <zeeshanak@gnome.org>.
 *
 * Author: Jussi Kukkonen <jku@openedhand.com>
 *         Zeeshan Ali (Khattak) <zeeshanak@gnome.org>
 *
 * This file is part of gupnp-vala.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

using GLib;
using GSSDP;

public class TestPublisher : ResourceGroup {
    public string resource_type { get; set construct; }
    public string usn { get; set construct; }
    public string location { get; set construct; }

    construct {
        this.add_resource_simple (this.resource_type,
                                  this.usn,
                                  this.location);
    }

    public TestPublisher() {
        Object (client: new Client (null, null),
                resource_type: "upnp:rootdevice",
                usn: "uuid:1234abcd-12ab-12ab-12ab-1234567abc12" +
                      "::upnp:rootdevice",
                location: "http://192.168.1.100/");
    }

    public static int main (string[] args) {
        TestPublisher publisher = new TestPublisher ();

        publisher.available = true;

        MainLoop loop = new MainLoop (null, false);
        loop.run();

        return 0;
    }
}
